import { EdaAdminPage } from './app.po';

describe('eda-admin App', () => {
  let page: EdaAdminPage;

  beforeEach(() => {
    page = new EdaAdminPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});

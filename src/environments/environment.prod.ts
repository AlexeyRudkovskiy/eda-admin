export const environment = {
  production: true,

  base_path: 'http://api.kingsmeal.com.ua/',

  api_version: 'v1',
  api_url: 'http://api.kingsmeal.com.ua/api',

  google_maps: {
    api_key: 'AIzaSyAOQ6b_Tv5MQ6Y1-Mw9T73QWFObHiFO6NA'
  },

  actionCable: 'api.kingsmeal.com.ua'

};

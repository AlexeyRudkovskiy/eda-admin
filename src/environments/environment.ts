// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,

  base_path: 'http://localhost:6202/',

  api_version: 'v1',
  api_url: 'http://localhost:6202/api',

  google_maps: {
    api_key: 'AIzaSyAOQ6b_Tv5MQ6Y1-Mw9T73QWFObHiFO6NA'
  },

  actionCable: 'localhost:6202'

  // api_url: 'http://139.59.146.232:6202/api'
  // api_url: 'http://93.175.221.144:6202/api'
};

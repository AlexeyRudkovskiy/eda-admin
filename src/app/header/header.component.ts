import { Component, OnInit } from '@angular/core';
import {APIService} from "../api.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public isLogoutVisible: boolean = false;

  constructor(private api: APIService) { }

  ngOnInit() {
    this.api.getTokenChangedObservable().subscribe(token => this.isLogoutVisible = token !== null);
  }

  public logout() {
    this.api.authToken = null;
  }

}

import { BrowserModule } from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
import { ItemComponent } from './sidebar/item/item.component';
import { HttpModule } from "@angular/http";
import { ShopsRoutingModule } from "./pages/shops/shops-routing.module";
import { DashboardRoutingModule } from "./pages/dashboard/dashboard-routing.module";
import { AuthService } from "./auth.service";
import { APIService } from "./api.service";
import { AuthLoginComponent } from './auth/login/login.component';
import { AuthGuard } from "./auth.guard";

import { MdButtonModule, MdInputModule, MdProgressSpinnerModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from "@angular/forms";
import { UiService } from "./ui.service";
import { RouterModule } from "@angular/router";
import { PagesModule } from "./pages/pages.module";
import { TranslationService } from "./services/translation.service";
import { NotificationService } from "./services/notification.service";
import { HttpInterceptor } from "./http-interceptor";
import { FormsService } from "./services/forms.service";
import { SidebarService } from "./sidebar/sidebar.service";
import { LocalStorage } from "./storages/local-storage";
import { CategoriesModule } from "./pages/categories/categories.module";
import { CategoriesRoutingModule } from "./pages/categories/categories-routing.module";
import {UiModule} from "./ui/ui.module";
import {WebSocketService} from "./services/websocket.service";

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    HeaderComponent,
    ItemComponent,
    AuthLoginComponent,
  ],
  imports: [
    FormsModule,
    MdButtonModule,
    MdInputModule,
    MdProgressSpinnerModule,
    BrowserAnimationsModule,

    PagesModule,

    BrowserModule,
    AppRoutingModule,
    HttpModule,
    RouterModule,

    DashboardRoutingModule,
    ShopsRoutingModule,

    CategoriesModule,
    CategoriesRoutingModule,
    UiModule,
  ],
  providers: [
    TranslationService,
    HttpInterceptor,
    APIService,
    NotificationService,
    AuthService,
    AuthGuard,
    UiService,
    FormsService,
    SidebarService,
    LocalStorage,
    WebSocketService,
    { provide: LOCALE_ID, useValue: "ru-RU" }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

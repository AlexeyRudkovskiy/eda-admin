import { Component, OnInit } from '@angular/core';
import {AuthService} from "./auth.service";
import {APIResponse} from "./apiresponse";
import {APIService} from "./api.service";
import {UiService} from "./ui.service";
import {HttpInterceptor} from "./http-interceptor";
import {SidebarService} from "./sidebar/sidebar.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public isHaveTabs: boolean = false;

  public foo: boolean = false;

  public title = 'app';

  public httpLoading: boolean = false;

  constructor(
      private auth: AuthService,
      private apiService: APIService,
      private ui: UiService,
      private httpInterceptor: HttpInterceptor,
      private sidebarService: SidebarService
  ) { }

  ngOnInit() {
    this.httpInterceptor.loadingState.subscribe(state => {
      this.httpLoading = state;
    });
  }

  ngAfterViewInit() {
    this.ui.isHaveContentAreaPaddings.subscribe(value => this.isHaveTabs = value);
  }

  private onSuccessfullyLogin(response: APIResponse) {
    this.apiService.authToken = response.data.remember_token;
  }

}

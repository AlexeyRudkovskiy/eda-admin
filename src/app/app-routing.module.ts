import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthLoginComponent} from "./auth/login/login.component";
import {DashboardIndexComponent} from "./pages/dashboard/index/index.component";
import {AuthGuard} from "./auth.guard";

const routes: Routes = [
  {
    path: '',
    component: DashboardIndexComponent,
    children: [],
    canActivate: [ AuthGuard ]
  },
  {
    path: 'dashboard',
    component: DashboardIndexComponent,
    canActivate: [ AuthGuard ],
    canActivateChild: [ AuthGuard ]
  },
  {
    path: 'auth/login',
    component: AuthLoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

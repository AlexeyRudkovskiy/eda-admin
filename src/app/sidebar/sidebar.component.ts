import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {SidebarService} from "./sidebar.service";
import {APIService} from "../api.service";
import {ItemComponent} from "./item/item.component";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input() current: string = 'dashboard';

  public sidebar = [];

  public shops = [];

  public isVisible: boolean = true;

  constructor(private sidebarService:SidebarService, private api: APIService) { }

  ngOnInit() {
    this.sidebarService.getSidebar().subscribe(item => this.sidebar.push(item));
    this.sidebarService.getShops().subscribe(shop => this.shops.push(shop));

    this.api.getTokenChangedObservable().subscribe(token => this.isVisible = token !== null);
  }

  public markAsSelected(item: ItemComponent) {
    this.shops.forEach(sidebarItem => sidebarItem.isActive = false);
    this.sidebar.forEach(sidebarItem => sidebarItem.isActive = false);
    item.isActive = true;
  }

}

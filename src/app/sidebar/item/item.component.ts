import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-sidebar-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  @Input() text: string;

  @Input() miIcon: string;

  @Input() badge: any;

  @Input() isActive: boolean = false;

  @Input() href: string;

  @Input() type: string = 'item';

  constructor(private router: Router) { }

  ngOnInit() {
  }

  public navigateTo() {
    this.router.navigateByUrl(this.href);
  }

  public get badgeValue() {
    const badgeValueAsNumber = typeof this.badge !== "number" ? Number(this.badge) : this.badge;

    if (badgeValueAsNumber === 0) {
      return null;
    }

    if (isNaN(badgeValueAsNumber)) {
      return this.badge;
    }

    return badgeValueAsNumber;
  }

  public markAsActive() {
    this.isActive = true;
  }

}

import { Injectable } from '@angular/core';
import { Http } from "@angular/http";

import 'rxjs/operator/map'
import {Observable} from "rxjs/Observable";
import {NotificationService} from "../services/notification.service";

import 'rxjs/add/operator/filter';
import {Observer} from "rxjs/Observer";
import {Subject} from "rxjs/Subject";
import {ReplaySubject} from "rxjs/ReplaySubject";
import {LocalStorage} from "../storages/local-storage";
import {WebSocketService} from "../services/websocket.service";
import {TranslationService} from "../services/translation.service";

@Injectable()
export class SidebarService {

  private separator: any = { type: 'separator' };

  private shops = [ /* empty */ ];

  private sidebar = [
    { label: 'app.sidebar.users', icon: 'people', href: '/users', badge: null },
    { label: 'app.sidebar.categories', icon: 'list_view', href: '/categories/shared', badge: null },
    { label: 'app.sidebar.advertisings', icon: 'picture_in_picture', href: '/advertising', badge: null },
    { label: 'app.sidebar.create_shop', icon: 'create', href: '/shops/create', badge: null },
    { label: 'app.sidebar.promo_codes', icon: 'card_giftcard', href: '/promo-codes', badge: null }
  ];

  private shopsSubject: ReplaySubject<any> = null;

  constructor(
    private notifications: NotificationService,
    private storage: LocalStorage,
    private webSocketService: WebSocketService,
    private translationService: TranslationService
  ) {
    this.notifications.eventsObservable
        .filter(event => event.tags.includes('shop.badge'))
        .subscribe(event => this.updateBadge(event));

    this.shopsSubject = new ReplaySubject();
    if (this.storage.has('shops')) {
      const shopsInStorage = JSON.parse(this.storage.get('shops'));
      if (localStorage.token.length > 4) {
        shopsInStorage
          .map(shop => {
            shop.badge = 0;
            return shop;
          })
          .forEach(shop => this.addShop(shop.label, shop.id, shop.badge));
      } else {
        localStorage.removeItem('shops');
      }
    }

    this.webSocketService.newMessageObservable
      .filter(order => order.identifier.channel === 'IncrementShopCounterChannel')
      .subscribe(order => this.incrementShopBadge(order));
  }

  public getSidebar () {
    return Observable.create(observer => {
      this.sidebar
        .map(item => {
          if ((item as any).type !== 'separator') {
            item.label = this.translationService.translate(item.label);
          }
          return item;
        })
        .forEach(item => observer.next(item))
    });
  }

  public getShops() {
    return this.shopsSubject;
  }

  public addShop(label: string, shop_id: number, badge: number = 0) {
    const shopObject = {
      label: label,
      shop_id: shop_id,
      icon: 'store',
      href: `/shops/${shop_id}/information`,
      badge: badge
    };

    this.shops.push(shopObject);
    this.shopsSubject.next(shopObject);
  }

  public recreateShopsSubject() {
    this.shopsSubject.unsubscribe();
    this.shopsSubject = new ReplaySubject();
    this.shops.splice(0, this.shops.length);
  }

  private updateBadge(event) {
    const sidebarItem: any = this.sidebar
        .filter(item => typeof (item as any).shop_id !== "undefined" && (item as any).shop_id === event.shop_id)
        .forEach(item => (item as any).badge += 1);
  }

  private incrementShopBadge(message: any) {
    let shopId: number = message.message.shop_id;
    let value: number = message.message.counter;

    this.shops
      .filter(shop => shop.shop_id === shopId)
      .forEach(shop => shop.badge = value);
  }

}

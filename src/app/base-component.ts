import {Subscription} from "rxjs/Subscription";
import {OnDestroy} from "@angular/core";

export abstract class BaseComponent implements OnDestroy {

    private subscriptions: Subscription[] = [];

    public registerSubscription(subscription: Subscription) {
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(): void {
        this.subscriptions = this.subscriptions
            .map(subscription => {
                subscription.unsubscribe();
                return null;
            })
            .filter(subscription => subscription != null);
    }

}
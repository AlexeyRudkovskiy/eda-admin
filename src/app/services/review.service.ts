import { Injectable } from '@angular/core';
import {APIService} from "../api.service";
import {Observable} from "rxjs/Observable";
import {Review} from "../models/review";
import {ReplaySubject} from "rxjs/ReplaySubject";

@Injectable()
export class ReviewService {

  public loadedEvent: ReplaySubject<any> = new ReplaySubject();

  public take: number = 15;

  public loaded: number = 0;

  constructor(
      private api: APIService
  ) { }

  public getProductReviews(id: number, take?: number, skip?: number): Observable<Review> {
    return this.api.get(`products/${id}/comments?take=${take || this.take}&skip=${skip || this.loaded}`)
        .map(response => response.json())
        .do(response => this.loadedEvent.next(response))
        .map(response => response.data)
        .do(response => this.loaded += response.length)
        .concatMap(review => review)
        .map(review => Review.fromObject(review));
  }

  public getReview() {

  }

}

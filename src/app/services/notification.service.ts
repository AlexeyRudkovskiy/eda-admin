import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {Observer} from "rxjs/Observer";

@Injectable()
export class NotificationService {

  public eventsObservable: Observable<any> = null;

  public eventsObserver: Observer<any> = null;

  constructor() {
    this.eventsObservable = Observable.create(observer => this.eventsObserver = observer).share();

    window.setTimeout(() => {

      let notification = {
        tags: [ 'shop.badge' ],
        message: 'app.order.new',
        shop_id: 1
      };

      this.eventsObserver.next(notification);

    }, 500);
  }

}

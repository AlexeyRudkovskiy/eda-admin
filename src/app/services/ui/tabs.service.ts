import {EventEmitter, Injectable} from '@angular/core';
import {ReplaySubject} from "rxjs/ReplaySubject";
import {TabItem} from "../../ui/tabs/item/item.component";

import 'rxjs/add/operator/publish'
import 'rxjs/add/operator/share'
import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";

@Injectable()
export class TabsService {

  private _tabsItemsReplaySubject: ReplaySubject<TabItem[]> = new ReplaySubject();

  private _shouldClear: EventEmitter<void> = new EventEmitter<void>();

  private _tabChanged: ReplaySubject<TabItem> = new ReplaySubject();

  constructor() {

  }

  get tabsItems(): ReplaySubject<TabItem[]> {
    return this._tabsItemsReplaySubject;
  }

  get shouldClear(): EventEmitter<void> {
    return this._shouldClear;
  }

  public addTabsItem(tabItem: TabItem): TabsService {
    this._tabsItemsReplaySubject.next([ tabItem ]);
    return this;
  }

  public addTabsItems(tabItems: TabItem[]): TabsService {
    this._tabsItemsReplaySubject.next(tabItems);
    return this;
  }

  public clearTabs(): TabsService {
    this._tabsItemsReplaySubject.complete();
    this._tabsItemsReplaySubject = new ReplaySubject();
    return this;
  }

  public getTabChangedObservable(): Observable<TabItem> {
    return this._tabChanged;
  }

  public resetTabChangedReplaySubject() {
    this._tabChanged.complete();
    this._tabChanged.unsubscribe();

    this._tabChanged = new ReplaySubject();
  }

  public changeTab(tab: TabItem) {
    this._tabChanged.next(tab);
  }

}

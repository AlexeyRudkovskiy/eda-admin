import {EventEmitter, Injectable} from '@angular/core';
import {APIService} from "../api.service";
import {Observable} from "rxjs/Observable";
import {PromoCode} from "../models/promo-code";

import 'rxjs/add/operator/delay'
import 'rxjs/add/observable/range'
import {APIResponse} from "../apiresponse";

@Injectable()
export class PromoCodesService {

  public skip: number = 0;

  public noMoreItemsEvent: EventEmitter<void> = new EventEmitter<void>();

  public promoCodesLoaded: EventEmitter<APIResponse> = new EventEmitter<APIResponse>();

  constructor(private apiService: APIService) { }

  public getAllPromoCodes(take: number = 15, skip: number = null): Observable<PromoCode> {
    skip = skip === null ? this.skip : skip;
    return this.apiService.get(`promo_codes/all?take=${take}&skip=${skip}`)
        .map(response => response.json())
        .do(response => this.processResponse(response))
        .map(response => response.data)
        .switchMap(promoCode => promoCode)
        .map(promoCode => PromoCode.fromObject<PromoCode>(promoCode));
  }

  public create(promoCode: PromoCode): Observable<void> {
    return this.apiService.post(`promo_codes`, promoCode.toObject())
        .map(response => response.json())
        .filter(response => response.response === 'success');
  }

  public delete(promoCode: PromoCode): Observable<void> {
    return this.apiService.delete(`promo_codes/${promoCode.id}`)
        .map(response => response.json())
        .filter(response => response.response === 'success');
  }

  public getShopPromoCodes(id: number, take: number = 15, skip: number = 0): Observable<PromoCode> {
    return this.apiService.get(`promo_codes/shop/${id}?take=${take}&skip=${skip}`)
      .map(response => response.json())
      .do(response => this.promoCodesLoaded.next(response))
      .map(response => response.data)
      .concatMap(item => item)
      .map(item => PromoCode.fromObject<PromoCode>(item));
  }

  private processResponse(response) {
    let recordsCount = response.data.length;
    this.skip += recordsCount;
    if (recordsCount < response.pagination.take) {
      this.noMoreItemsEvent.emit();
    }
  }

}

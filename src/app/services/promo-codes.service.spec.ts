import { TestBed, inject } from '@angular/core/testing';

import { PromoCodesService } from './promo-codes.service';

describe('PromoCodesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PromoCodesService]
    });
  });

  it('should be created', inject([PromoCodesService], (service: PromoCodesService) => {
    expect(service).toBeTruthy();
  }));
});

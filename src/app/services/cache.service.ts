import { Injectable } from '@angular/core';
import {LocalStorage} from "../storages/local-storage";
import {Storage} from "../storages/storage";
import {Product} from "../models/product";

@Injectable()
export class CacheService {

  private storage: Storage = new LocalStorage();

  constructor() { }

  public saveProduct(productId: number, product: Product) {
    this.storage.save(`product-${productId}`, product);
  }

  public getProduct(productId: number): Product {
    return Product.fromObject<Product>(JSON.parse(this.storage.get(`product-${productId}`)));
  }

}

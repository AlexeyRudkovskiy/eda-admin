import {EventEmitter, Inject, Injectable} from '@angular/core';
import {APIService} from "../api.service";
import {Observable} from "rxjs/Observable";
import {Shop} from "../models/shop";

import 'rxjs/add/operator/concatMap'
import 'rxjs/add/operator/do'
import {ReplaySubject} from "rxjs/ReplaySubject";
import {FormsService} from "./forms.service";
import {SidebarService} from "../sidebar/sidebar.service";

export interface CreateShopParams {
  name: string;
  deliveryPrice: number;
  deliveryCurrency: string;
  deliveryDiscount: number;
  shopImage?: File;
  deliveryTime: number;
}

@Injectable()
export class ShopService {

  public take: number = 15;

  public loadedEvent: ReplaySubject<any> = new ReplaySubject();

  public currentShopId: number = -1;

  public currentShop: Shop = null;

  public shopReplaySubject: ReplaySubject<Shop> = null;

  constructor(
    private api: APIService,
    private forms: FormsService,
    private sidebarService: SidebarService
  ) {
    this.shopReplaySubject = new ReplaySubject();
  }

  public getList(skip: number = 0, take: number = 15, fields: string[] = []): Observable<Shop> {
    return this.api.get(`shops?take=${take}&skip=${skip}&category=-1&fields=${fields.join(',')}`)
        .map(response => response.json())
        .do(response => this.loadedEvent.next(response))
        .map(response => response.data)
        .concatMap(item => item)
        .map(item => Shop.fromObject<Shop>(item))
        .share();
  }

  public getShop(id?: number): Observable<Shop> {
    if (this.currentShop !== null && this.currentShop.id === id) {
      return this.shopReplaySubject;
    }

    return this.api.get(`shops/${id}`)
        .map(response => response.json())
        .do(response => this.loadedEvent.next(response))
        .map(response => response.data)
        .map(shop => Shop.fromObject<Shop>(shop))
        .do(shop => this.recreateShopReplaySubject())
        .do(shop => this.currentShopId = shop.id)
        .do(shop => this.currentShop = shop)
        .do(shop => this.shopReplaySubject.next(shop));
  }

  public getCurrent(): ReplaySubject<Shop> {
    return this.shopReplaySubject;
  }

  public create(args: CreateShopParams): Observable<Shop> {
    const postData = this.forms.convertObjectToFormData(args);

    return this.api.post(`shops`, postData)
      .map(response => response.json())
      .map(response => response.data)
      .map(shop => Shop.fromObject<Shop>(shop))
      .do(shop => this.sidebarService.addShop(shop.shopName, shop.id));
  }

  public update(id: number, args: CreateShopParams): Observable<Shop> {
    return this.api.put(`shops/${id}`, args)
      .map(response => response.json())
      .map(response => response.data)
      .map(shop => Shop.fromObject<Shop>(shop));
  }

  private recreateShopReplaySubject() {
    if (this.shopReplaySubject !== null && this.currentShop !== null) {
      this.shopReplaySubject.unsubscribe();
      this.shopReplaySubject = new ReplaySubject();
    }
  }

}

import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {Observable} from "rxjs/Observable";

@Injectable()
export class MapsService {

  constructor() { }

  public getStaticMap(width: number, height: number, zoom: number, location: string): Observable<string> {
    return Observable.of(`https://maps.googleapis.com/maps/api/staticmap?center=${location}`
        + `&zoom=${zoom}`
        + `&size=${width}x${height}`
        + `&maptype=roadmap`
        + `&markers=${location}`
        + `&key=${environment.google_maps.api_key}`);
  }

}

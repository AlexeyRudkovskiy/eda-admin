import {Inject, Injectable, InjectionToken, Injector} from '@angular/core';
import {Http} from "@angular/http";
import {Observable} from "rxjs/Observable";

declare const require;
const translations = JSON.parse(require(`raw-loader!./../locale/locale.json`));

@Injectable()
export class TranslationService {

  public locale: string = 'ru';

  constructor() {  }

  public translate(key): string {
    const keySegments = key.split('.');
    let value = translations[this.locale];

    try {
      while (keySegments.length > 0) {
        value = value[keySegments.shift()];
        if (typeof value === "undefined") {
          value = key;
          break;
        }
      }
    } catch (exception) {
      value = key;
    }

    return value;
  }

}

import { Injectable } from '@angular/core';
import {APIService} from "../api.service";
import {Observable} from "rxjs/Observable";
import {Size} from "../models/size";

import {Option} from "../models/option";
import {Price} from "../models/price";

import 'rxjs/add/observable/of'
import 'rxjs/add/observable/from'
import {Product} from "../models/product";

export interface CreateSizeInterface {
  title: string;
  options: number[];
  price: Price;
}

@Injectable()
export class SizeService {

  constructor(protected api: APIService) { }

  public getShopOptions(shopId: number): Observable<Option> {
    const firstOption = new Option();
    firstOption.description = "First option description";
    firstOption.id = 1;
    firstOption.name = "first option";
    firstOption.price = new Price;
    firstOption.price.amount = 10;
    firstOption.price.currency = 'uah';
    firstOption.price.discount = 0;

    return Observable.from([firstOption]);
  }

  public create(args: CreateSizeInterface, shopId: number, productId: number): Observable<Size> {
    return this.api.post(`shops/${shopId}/products/${productId}/sizes`, args)
      .map(response => response.json())
      .map(response => response.data)
      .map(size => Size.fromObject(size));
  }

  public getSize(sizeId: number, product: Product): Observable<Size> {
    const sizes = product.sizes.filter(size => size.id === sizeId);
    return Observable.from(sizes);
  }

  public update(shopId: number, size: Size, optionsIds: number[]): Observable<Size> {
    const data = size.toObject();
    data.options = optionsIds;

    return this.api.put(`shops/${shopId}/sizes/${size.id}`, data)
      .map(response => response.json())
      .map(response => response.data)
      .map(response => Size.fromObject<Size>(response));
  }

  public delete(shopId: number, productId: number, size: Size): Observable<boolean> {
    return this.api.delete(`shops/${shopId}/products/${productId}/sizes/${size.id}`)
      .map(response => response.json())
      .map(response => true); // setting true if got response
  }

}

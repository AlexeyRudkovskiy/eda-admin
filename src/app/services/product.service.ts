import {Inject, Injectable} from '@angular/core';
import {APIService} from "../api.service";
import {Observable} from "rxjs/Observable";
import {Product} from "../models/product";
import {Shop} from "../models/shop";
import {ReplaySubject} from "rxjs/ReplaySubject";
import {ShopService} from "./shop.service";
import {Subject} from "rxjs/Subject";
import {FormsService} from "./forms.service";

@Injectable()
export class ProductService {

  public loadedEvent: ReplaySubject<any> = new ReplaySubject();

  public currentProduct: ReplaySubject<Product> = new ReplaySubject();

  public updateProduct: Subject<Product> = new Subject();

  public take: number = 15;

  public loaded: number = 0;

  private skip: number = 0;

  // private _currentProduct: Product = null;

  private _currentProduct: Product = null;

  constructor(
      private api: APIService,
      private forms: FormsService,
      private shopsService: ShopService
  ) { }

  public getShopProducts(id: number, take: number = null, skip: number = null): Observable<Product> {
    skip = skip === null ? this.skip : skip;

    return this.api.get(`shops/${id}/products?take=${take || this.take}&skip=${skip}`)
      .map(response => response.json())
      .do(rawResponse => this.loadedEvent.next(rawResponse))
      .map(response => response.data)
      .do(items => this.loaded += items.length)
      .concatMap(item => item)
      .map(item => Product.fromObject<Product>(item));
  }

  public getProduct(shopId: number, id: number): Observable<Product> {
    if (this._currentProduct !== null) {
      return Observable.of(this._currentProduct);
    }

    return this.api.get(`shops/${shopId}/products/${id}?fields=categories`)
      .map(response => response.json())
      .do(response => this.loadedEvent.next(response))
      .map(response => response.data)
      .map(response => Product.fromObject<Product>(response))
      .flatMap(product => this.appendShopToProduct(shopId, product))
      .do(product => this.currentProduct.next(product))
      .do(product => this.currentProduct.complete())
      .do(product => this._currentProduct = product);
  }

  public recreateCurrentProductReplaySubject(product: Product = null) {
    this.currentProduct.unsubscribe();
    this.currentProduct = new ReplaySubject();
  }

  public save(data: any, shopId: number, id: number = null): Observable<Product> {
    const observable = id !== null
        ? this.api.put(`shops/${shopId}/products/${id}`, data)
        : this.api.post(`shops/${shopId}/products`, data);

    return observable
        .map(response => response.json())
        .map(response => response.data)
        .map(product => Product.fromObject<Product>(product))
        .flatMap(product => this.appendShopToProduct(shopId, product))
        .do(product => this.updateProduct.next(product));
  }

  public create(shopId: number, product: Product, photo: File = null): Observable<Product> {
    let postData: any = product.toObject();

    delete postData['photo'];

    if (photo !== null) {
      postData = this.forms.convertObjectToFormData(postData);
      postData.append('photo', photo);
    }


    return this.api.post(`shops/${shopId}/products`, postData)
      .map(response => response.json())
      .map(response => response.data)
      .map(product => Product.fromObject(product));
  }

  public delete(shopId: number, productId: number): Observable<boolean> {
    return this.api.delete(`shops/${shopId}/products/${productId}`)
      .map(response => response.json())
      .map(response => response.data)
      .map(response => true); // setting true if got response
  }

  private appendShopToProduct(shopId: number, product: Product): Observable<Product> {
    return Observable.create(observer => {
      this.shopsService.getShop(shopId)
          .do(shop => product.shop = shop)
          .do(() => observer.next(product))
          .subscribe(() => observer.complete());
    });
  }

}

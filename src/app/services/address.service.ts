import { Injectable } from '@angular/core';
import {APIService} from "../api.service";
import {Observable} from "rxjs/Observable";
import {Address, City, District} from "../models/address";
import {Subject} from "rxjs/Subject";
import {APIResponse} from "../apiresponse";
import {Location} from "../models/location";

export interface CreateAddressArguments {
  address: string;
  district: District;
  location: Location;
  opensFrom: string;
  closesAt: string;
}

@Injectable()
export class AddressService {

  public take: number = 15;

  public skip: number = 0;

  public loadedEvent: Subject<APIResponse> = new Subject();

  constructor(private api: APIService) { }

  public getShopAddresses(shopId: number, take: number = -1, skip: number = -1): Observable<Address> {
    take = take > -1 ? take : this.take;
    skip = skip > -1 ? skip : this.skip;

    return this.api.get(`shops/${shopId}/addresses?take=${take}&skip=${skip}`)
      .map(response => response.json())
      .do(response => this.loadedEvent.next(response))
      .map(response => response.data)
      .concatMap(address => address)
      .map(address => Address.fromObject<Address>(address));
  }

  public getCities(): Observable<City> {
    return this.api.get(`cities?take=15&skip=0`)
      .map(response => response.json())
      .map(response => response.data)
      .concatMap(city => city)
      .map(city => City.fromObject<City>(city));
  }

  public getDistricts(city: City): Observable<District> {
    return this.api.get(`cities/${city.id}/districts?take=15&skip=0`)
      .map(response => response.json())
      .map(response => response.data)
      .concatMap(district => district)
      .map(district => District.fromObject<District>(district));
  }

  public create(shopId: number, createAddressArgs: CreateAddressArguments): Observable<Address> {
    const addressArgs = {
      lat: createAddressArgs.location.lat,
      lng: createAddressArgs.location.lng,
      address: createAddressArgs.address,
      closes_at: createAddressArgs.closesAt,
      opens_from: createAddressArgs.opensFrom,
      district_id: createAddressArgs.district.id
    };

    return this.api.post(`shops/${shopId}/address`, addressArgs)
      .map(response => response.json())
      .map(response => response.data)
      .map(address => Address.fromObject<Address>(address));
  }

  public delete(shopId: number, address: Address): Observable<APIResponse> {
    return this.api.delete(`shops/${shopId}/address/${address.id}`)
      .map(response => response.json())
      .map(response => response as APIResponse);
  }

}

import { Injectable } from '@angular/core';
import {APIService} from "../api.service";
import {Label} from "../models/label";
import {Observable} from "rxjs/Observable";

@Injectable()
export class LabelsService {

  constructor(private api: APIService) { }

  public getAllShopLabels(shopId: number): Observable<Label> {
    return this.api.get(`shops/${shopId}/labels`)
      .map(response => response.json())
      .map(response => response.data)
      .switchMap(item => item)
      .map(item => Label.fromObject<Label>(item));
  }

  public delete(shopId: number, label: Label) {
    return this.api.delete(`shops/${shopId}/labels/${label.id}`);
  }

  create(shopId: number, label: Label, productId: number = -1): Observable<Label> {

    let path = `shops/${shopId}/labels`;
    if (productId > -1) {
      path = `shops/${shopId}/products/${productId}/labels`;
    }

    return this.api.post(path, label.toObject())
      .map(response => response.json())
      .map(response => response.data)
      .map(response => Label.fromObject<Label>(response));
  }

}

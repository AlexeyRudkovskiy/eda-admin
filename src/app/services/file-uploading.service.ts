import { Injectable } from '@angular/core';
import {APIService} from "../api.service";
import {Image} from "../models/image";
import {Observable} from "rxjs/Observable";
import {APIResponse} from "../apiresponse";
import {Observer} from "rxjs/Observer";
import {ProductService} from "./product.service";

@Injectable()
export class FileUploadingService {

  private targetPathPrefixes = {
    product: 'products',
    shop: 'shops'
  };

  private progressObserver: Observer<number> = null;

  public progressObservable: Observable<number> = Observable.create(observer => this.progressObserver = observer).share();

  constructor(
    private api: APIService,
    private productService: ProductService
  ) { }

  public uploadFile(shopId: number, productId: number, photo: File, field: string = null, findPhotoCallback: any = null): Observable<Image> {
    let targetUrl = `shops/${shopId}`;
    if (productId > 0) {
      targetUrl += `/products/${productId}`;
    }

    return this.createUploadObserver(targetUrl, photo, field)
        .map(response => response.data)
        .map(product => findPhotoCallback !== null ? findPhotoCallback.call(window, product) : product.photo)
        .map(photo => Image.fromObject(photo));

    // return this.api.put(targetUrl, formData)
    //     .map(response => response.json())
    //     .map(response => response.data)
    //     .map(response => response.photo)
    //     .map(photo => Image.fromObject(photo));
        // .map(response => );
  }

  private createUploadObserver(url: string, file: File, field: string = null): Observable<APIResponse> {
    return Observable.create(observer => {
      const formData = new FormData();
      const xhr = new XMLHttpRequest();
      xhr.open('PUT', this.api.url + url, true);
      xhr.setRequestHeader("Authorization", this.api.authToken);

      xhr.addEventListener('readystatechange', event => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            observer.next(JSON.parse(xhr.response));
            observer.complete();
          } else {
            observer.error(xhr.response);
          }
        }
      });

      xhr.upload.addEventListener('progress', event => {
        const progress = Math.round(event.loaded / event.total * 100);
        this.progressObserver.next(progress);
      });

      formData.append('photo', file);
      if (field !== null) {
        formData.append('field', field);
      }
      xhr.send(formData);
    });
  }

}

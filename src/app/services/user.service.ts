import { Injectable } from '@angular/core';
import {APIService} from "../api.service";
import {Observable} from "rxjs/Observable";
import {User} from "../models/user";
import {ReplaySubject} from "rxjs/ReplaySubject";
import {FormsService} from "./forms.service";

import * as shajs from 'sha.js';

@Injectable()
export class UserService {

  public take: number = 15;

  public skip: number = 0;

  public loadedEvent: ReplaySubject<any> = new ReplaySubject();

  constructor(
    private api: APIService,
    private formsService: FormsService
  ) { }

  public administrators(take: number = null, skip: number = null): Observable<User> {
    take = take === null ? this.take : take;
    skip = skip === null ? this.skip : skip;

    return this.api.get(`users/administrators?take=${take}&skip=${skip}`)
      .map(response => response.json())
      .do(response => this.loadedEvent.next(response))
      .do(response => this.skip += response.data.length)
      .map(response => response.data)
      .concatMap(user => user)
      .do(data => console.log('data', data))
      .map(user => User.fromObject<User>(user))
      .share()
  }

  getUser(id: number): Observable<User> {
    if (id === 0 || id < 1) {
      return Observable.create(observer => {
        observer.next(new User());
        observer.complete();
      });
    }

    return this.api.get(`users/${id}`)
      .map(response => response.json())
      .map(response => response.data)
      .map(user => User.fromObject<User>(user));
  }

  save(id: number, user: any): Observable<User> {
    if (typeof user.password !== "undefined" && user.password !== null) {
      const passwordHash = shajs('sha1').update(user.password).digest('hex');
      user.password = passwordHash;
    }

    const data = this.formsService.convertObjectToFormData(user);

    if (typeof user.avatar !== "undefined") {
      data.append('avatar', user.avatar);
    }

    const endpoint = id >= 1 ? this.api.put(`users/${id}`, data) : this.api.post(`users`, data);

    return endpoint
      .map(response => response.json())
      .map(response => response.data)
      .map(user => User.fromObject<User>(user));
  }
}

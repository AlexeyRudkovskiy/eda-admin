import { TestBed, inject } from '@angular/core/testing';

import { FileUploadingService } from './file-uploading.service';

describe('FileUploadingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FileUploadingService]
    });
  });

  it('should be created', inject([FileUploadingService], (service: FileUploadingService) => {
    expect(service).toBeTruthy();
  }));
});

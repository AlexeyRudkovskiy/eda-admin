import { Injectable } from '@angular/core';
import {APIService} from "../api.service";
import {Observable} from "rxjs/Observable";
import {Observer} from "rxjs/Observer";
import { environment } from "../../environments/environment";

@Injectable()
export class WebSocketService {

  private currentTimer = null;

  private connection: any = null;

  private newMessageObserver: Observer<any> = null;

  public newMessageObservable: Observable<any> = null;

  constructor(private apiService: APIService) {
    this.connect();
    this.newMessageObservable = Observable.create(observer => this.newMessageObserver = observer);
  }

  private connect() {
      this.apiService.getTokenChangedObservable()
        .subscribe(token => this.connectWithToken(token));
  }

  private connectWithToken(token) {
      if (token !== null) {
          if (this.connection !== null) {
            this.connection.close();
          }

          this.connection = new WebSocket(`ws:///${environment.actionCable}/cable`);
          this.connection.onopen = () => this.onOpen(token);
          this.connection.onmessage = (message) => this.onMessage(message);
          this.connection.onclose = () => clearInterval(this.currentTimer);
      }
  }

  private onOpen(token: string) {
      const msg = {
          command: 'subscribe',
          identifier: JSON.stringify({
              channel: 'IncrementShopCounterChannel',
              token: token
          }),
      };
      this.connection.send(JSON.stringify(msg));

      // this.currentTimer = setInterval(() => {
      //   this.connection.send(JSON.stringify({ type: "pong", message: +(new Date()) }))
      // }, 3000);
  }

  private onMessage(message) {
    const cableData = JSON.parse(message.data);
    if (typeof cableData.identifier !== "undefined" && typeof cableData.message !== "undefined") {
      const identifier = JSON.parse(cableData.identifier);
      const cableMessage = JSON.parse(cableData.message);
      this.newMessageObserver.next({
        identifier, message: cableMessage
      });
    }
  }

}

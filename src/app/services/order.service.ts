import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {Order} from "../models/order";
import {APIService} from "../api.service";
import {Subject} from "rxjs/Subject";
import {APIResponse} from "../apiresponse";

@Injectable()
export class OrderService {

  public take: number = 15;

  public skip: number = 0;

  public loadedEvent: Subject<APIResponse> = new Subject();

  private isUpdatingStatus: boolean = false;

  constructor(private api: APIService) { }

  public getShopOrders(shopId: number, take: number = -1, skip: number = -1): Observable<Order> {
    take = take > -1 ? take : this.take;
    skip = skip > -1 ? skip : this.skip;

    return this.api.get(`shops/${shopId}/orders?take=${take}&skip=${skip}&fields=list,status,statuses`)
      .map(response => response.json())
      .do(response => this.loadedEvent.next(response))
      .do(response => this.skip += response.pagination.size)
      .map(response => response.data)
      .concatMap(order => order)
      .map(order => Order.fromObject<Order>(order));
  }

  public getOrder(shopId: number, orderId: number): Observable<Order> {
    return this.api.get(`shops/${shopId}/orders/${orderId}`)
      .map(response => response.json())
      .map(response => response.data)
      .map(order => Order.fromObject<Order>(order));
  }

  public updateStatus(shopId: number, order: Order): Observable<Order> {
    if (this.isUpdatingStatus) {
      return Observable.empty();
    }

    this.isUpdatingStatus = true;

    let params = { completed: true };

    return this.api.patch(`shops/${shopId}/orders/${order.id}`, params)
      .map(response => response.json())
      .map(response => response.data)
      .do(data => this.isUpdatingStatus = false)
      .map(updatedOrder => Order.fromObject<Order>(updatedOrder))
      .map(updatedOrder => this.processStatuses(order, updatedOrder))
      .catch((error, caught) => this.processUpdateStatusError(error, caught));
  }

  private processStatuses(order: Order, updated: Order): Order {
    if (updated.processing.done) {
      order.processing = updated.processing;
    }

    if (updated.sent.done) {
      order.sent = updated.sent;
    }

    if (updated.received.done) {
      order.received = updated.received;
    }

    return order;
  }

  private processUpdateStatusError(error, caught) {
    this.isUpdatingStatus = false;
    return Observable.throw(error);
  }

}

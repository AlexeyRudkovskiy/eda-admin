import { Injectable } from '@angular/core';
import {APIService} from "../api.service";
import {Observable} from "rxjs/Observable";
import {Category} from "../models/category";
import {Subject} from "rxjs/Subject";
import {APIResponse} from "../apiresponse";

@Injectable()
export class CategoryService {

  public take: number = 15;

  public skip: number = 0;

  public loadedEvent: Subject<APIResponse> = new Subject<APIResponse>();

  constructor(
      private api: APIService
  ) { }

  public getShopCategories(shopId: number, take: number = -1, skip: number = -1): Observable<Category> {
      take = take > -1 ? take : this.take;
      skip = skip > -1 ? skip : this.skip;

      return this.api.get(`shops/${shopId}/categories?take=${take}&skip=${skip}&fields=id,name,shop_id`)
        .map(response => response.json())
        .do(response => this.skip += response.pagination.size)
        .do(response => this.loadedEvent.next(response))
        .map(response => response.data)
        .concatMap(item => item)
        .map(category => Category.fromObject<Category>(category));
  }

  public getCategory(shopId: number, categoryId: number): Observable<Category> {
    return this.api.get(`shops/${shopId}/categories/${categoryId}?fields=id,name`)
      .map(response => response.json())
      .map(response => response.data)
      .map(category => Category.fromObject<Category>(category));
  }

  public getSharedCategories(shopId: number) {
    const path = shopId > 0 ? `categories/shared/${shopId}` : `categories/shared`;
    return this.api.get(path)
      .map(response => response.json())
      .map(response => response.data)
      .concatMap(category => category)
      .map(category => Category.fromObject<Category>(category));
  }

  public createCategory(shopId: number, name: string): Observable<Category> {
    return this.api.post(`shops/${shopId}/categories`, { name })
      .map(response => response.json())
      .map(response => response.data)
      .map(category => Category.fromObject<Category>(category));
  }

  public createShared(category: Category): Observable<Category> {
    return this.api.post(`categories/shared`, category.toObject())
      .map(response => response.json())
      .map(response => response.data)
      .map(response => Category.fromObject(response));
  }

  public delete(shopId: number, category: Category): Observable<boolean> {
    return this.api.delete(`shops/${shopId}/categories/${category.id}`)
      .map(response => response.json())
      .map(response => response.response === 'success');
  }

  public deleteShared(category: Category): Observable<Category> {
    return this.api.delete(`categories/shared/${category.id}`)
      .map(response => response.json())
      .map(response => response.response)
      .filter(response => response === 'success')
      .catch((err, caught) => caught)
      .map(response => category);
  }

  public update(shopId: number, category: Category): Observable<boolean> {
    return this.api.put(`shops/${shopId}/categories/${category.id}`, category.toObject())
      .map(response => response.json())
      .map(response => response.response === 'success');
  }

  public updateShared(category: Category): Observable<Category> {
    return this.api.put(`categories/shared/${category.id}`, category.toObject())
      .map(response => response.json())
      .map(response => response.data)
      .map(response => Category.fromObject<Category>(response));
  }

  public useCategory(shopId: number, category: Category): Observable<any> {
    return this.api.post(`categories/${category.id}/use/${shopId}`, {  })
      .map(response => response.json())
      .map(response => response.data)
      .filter(response => response)
      .catch((err, caught) => caught);
  }

  public unuseCategory(shopId: number, category: Category) {
    return this.api.delete(`categories/${category.id}/use/${shopId}`)
      .map(response => response.json())
      .map(response => response.data)
      .filter(response => response)
      .catch((err, caught) => caught);
  }

}

import {EventEmitter, Injectable} from '@angular/core';
import {APIService} from "../api.service";
import {Observable} from "rxjs/Observable";
import {Option} from "../models/option";
import {APIResponse} from "../apiresponse";

import 'rxjs/add/operator/catch'
import 'rxjs/add/observable/throw'
import {Subject} from "rxjs/Subject";

@Injectable()
export class OptionService {

  public take: number = 15;

  public skip: number = 0;

  public onLoaded: Subject<APIResponse> = new Subject<APIResponse>();

  constructor(
      private api: APIService
  ) { }

  public getShopOptions(shopId: number, skip: number = -1, take: number = -1): Observable<Option> {
    return this.api.get(`shops/${shopId}/options?take=${take > -1 ? take : this.take}&skip=${skip > -1 ? skip : this.skip}`)
        .map(response => response.json())
        .do(response => this.skip += response.data.length)
        .do(response => this.onLoaded.next(response))
        .map(response => response.data)
        .concatMap(option => option)
        .catch(error => this.catchError(error))
        .map(option => Option.fromObject(option));
  }

  public getAllShopOptions(shopId: number): Observable<Option> {
    return this.getShopOptions(shopId, 0, 99999);
  }

  public getShopOption(shopId: number, optionId: number): Observable<Option> {
    return this.api.get(`shops/${shopId}/options/${optionId}`)
        .map(response => response.json())
        .map(response => response.data)
        .map(option => Option.fromObject(option));
  }

  public update(shopId: number, option: Option): Observable<Option> {
    return this.api.put(`shops/${shopId}/options/${option.id}`, option)
        .map(response => response.json())
        .map(response => response.data)
        .map(option => Option.fromObject(option));
  }

  public create(shopId: number, option: Option): Observable<Option> {
    return this.api.post(`shops/${shopId}/options`, option)
      .map(response => response.json())
      .map(response => response.data)
      .map(option => Option.fromObject(option));
  }

  public delete(shopId: number, option: Option) {
    return this.api.delete(`shops/${shopId}/options/${option.id}`)
      .map(response => response.json())
      .map(response => response.data)
      .map(response => true); // setting true if got response
  }

  public catchError(error: Response): Observable<Error> {
    return Observable.throw(new Error(((error.json() as any) as APIResponse).data.message));
  }

}

import { Injectable } from '@angular/core';
import {HttpInterceptor} from "../http-interceptor";
import {APIService} from "../api.service";
import {Advertising} from "../models/ads";
import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";
import {APIResponse} from "../apiresponse";

@Injectable()
export class AdvertisingService {

  public loadedEvent: Subject<APIResponse> = new Subject();

  public take: number = 15;

  public skip: number = 0;

  constructor(private api: APIService) { }

  public getAdsList(take: number = -1, skip: number = -1): Observable<Advertising> {
    take = take > -1 ? take : this.take;
    skip = skip > -1 ? skip : this.skip;

    return this.api.get(`advertisings?take=${take}&skip=${skip}`)
      .map(response => response.json())
      .do(response => this.loadedEvent.next(response))
      .map(response => response.data)
      .concatMap(ads => ads)
      .map(ads => Advertising.fromObject<Advertising>(ads));
  }

  public getAdvertising(id: number): Observable<Advertising> {
    return this.api.get(`advertisings/${id}`)
      .map(response => response.json())
      .map(response => response.data)
      .do(response => console.log(response))
      .map(ads => Advertising.fromObject<Advertising>(ads))
      .do(ads => console.log(ads));
  }

  public create(shop_id: string, name: string, isActive: boolean, title: string, content: string, actionType: string, actionAttributes: any, image: File): Observable<Advertising> {
    const formData = new FormData;
    formData.append('name', name);
    formData.append('is_active', `${isActive}`);
    formData.append('title', title);
    formData.append('content', content);
    formData.append('actionType', actionType);
    formData.append('actionAttributes', JSON.stringify(actionAttributes));
    formData.append('shop_id', shop_id);
    formData.append('image', image);

    return this.api.post(`advertisings`, formData)
      .map(response => response.json())
      .map(response => response.data)
      .map(ads => Advertising.fromObject<Advertising>(ads));
  }

  public update(id: number, shop_id: string, name: string, isActive: boolean, title: string, content: string, actionType: string, actionAttributes: any, image: File): Observable<Advertising> {
    const formData = new FormData;
    formData.append('name', name);
    formData.append('is_active', `${isActive}`);
    formData.append('title', title);
    formData.append('content', content);
    formData.append('actionType', actionType);
    formData.append('actionAttributes', JSON.stringify(actionAttributes));
    formData.append('shop_id', shop_id);
    if (image !== null) {
      formData.append('image', image);
    }

    return this.api.put(`advertisings/${id}`, formData)
      .map(response => response.json())
      .map(response => response.data)
      .map(ads => Advertising.fromObject<Advertising>(ads));
  }

  public delete(ads: Advertising): Observable<boolean> {
    return this.api.delete(`advertisings/${ads.id}`)
      .map(response => response.json())
      .map(response => response.response === 'success');
  }

}

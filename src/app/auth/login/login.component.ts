import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../auth.service";
import {APIService} from "../../api.service";
import {Router} from "@angular/router";
import {LocalStorage} from "../../storages/local-storage";

@Component({
  selector: 'app-auth-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class AuthLoginComponent implements OnInit {

  public message: String = "";

  public phone: string;

  public password: string;

  constructor(
      private authService: AuthService,
      private api: APIService,
      private router: Router,
      private storage: LocalStorage
  ) { }

  ngOnInit() {
      if (this.api.authToken !== null) {
          location.href = "/";
      }
  }

  public onLoginSubmit() {
      this.authService.authUsingPhoneAndPassword(this.phone, this.password)
          .subscribe(response => {
              if (typeof response.data.user !== "undefined" && typeof response.data.user.shops !== "undefined") {
                this.storage.save('shops', JSON.stringify(response.data.user.shops));
              }
              location.href = '/';
              localStorage.token = response.data.remember_token;
          }, error => this.message = "Error");
  }

}

import { Injectable } from '@angular/core';
import { Http, Headers, Response } from "@angular/http";
import { environment } from "../environments/environment";
import {Observable} from "rxjs/Observable";

import 'rxjs/add/operator/map'
import { APIResponse } from "./apiresponse";
import {Observer} from "rxjs/Observer";
import {ConnectableObservable} from "rxjs/Rx";

import 'rxjs/add/operator/share'
import 'rxjs/add/observable/empty'
import {Router} from "@angular/router";
import {Subject} from "rxjs/Subject";
import {HttpInterceptor} from "./http-interceptor";

@Injectable()
export class APIService {

  private apiVersion: string;

  private apiUrl: string;

  private _authToken: string = null;

  private _tempAuthToken: string = null;

  private tokenChangedConnectableObservable:ConnectableObservable<string> = null;

  private tokenChangedObserver:Observer<string> = null;

  constructor(private http: HttpInterceptor, private router: Router) {
    this.apiUrl = environment.api_url;
    this.apiVersion = environment.api_version;

    this.tokenChangedConnectableObservable = Observable.create(observer => {
      this.tokenChangedObserver = observer;
      observer.next(null);
    }).share();

    let token = localStorage.getItem('token');
    if (token != null) {
      this._tempAuthToken = token;

      this.get('check_token')
          .map(response => response.json())
          .subscribe(response => {
            if (this.tokenChangedObserver !== null) {
              this.tokenChangedObserver.next(token);
            }
            this._authToken = token;
          }, this.checkTokenFailed.bind(this));
    }
  }

  public get url(): string {
    return this.apiUrl + "/" + this.apiVersion + "/";
  }

  public get(url: string): Observable<Response> {
    let headers = this.createHeaders();
    return this.http.get(this.url + url, { headers });
  }

  public post(url: string, body: any): Observable<Response> {
    let headers = this.createHeaders();
    return this.http.post(this.url + url, body, { headers });
  }

  public put(url: string, body: any): Observable<Response> {
    let headers = this.createHeaders();
    return this.http.put(this.url + url, body, { headers });
  }

  public delete(url: string): Observable<Response> {
    let headers = this.createHeaders();
    return this.http.delete(this.url + url, { headers });
  }

  public patch(url: string, body: any): Observable<Response> {
    let headers = this.createHeaders();
    return this.http.patch(this.url + url, body, { headers });
  }

  public getTokenChangedObservable() {
    return this.tokenChangedConnectableObservable;
  }

  public createHeaders() {
    const headers = new Headers();
    const tempAuthToken = this.tempAuthToken;
    if (this.authToken !== null || tempAuthToken !== null) {
      headers.append("Authorization", this.authToken || tempAuthToken);
    }
    return headers;
  }

  get authToken(): string {
    return this._authToken;
  }

  set authToken(value: string) {
    localStorage.setItem('token', value);
    this.tokenChangedObserver.next(value);
    this._authToken = value;

    if (value === null) {
      this.router.navigate([ '/auth/login' ]);
    }
  }

  public get tempAuthToken(): string {
    return this._tempAuthToken;
  }

  private checkTokenFailed(response: Response) {
    const apiResponse: APIResponse =  APIResponse.fromResponse(response);
    if (apiResponse.code === 419) {
      console.error("Bad token. Response and messages attached", apiResponse, apiResponse.data.message);
      this.tokenChangedObserver.next(null);

      localStorage.setItem('token', null);
      if (!((location.pathname as string).indexOf('/auth') === 0)) {
        this.router.navigate([ '/auth/login' ]);
      }
    }
  }

}

import { Response } from '@angular/http'

export interface IPagination {

    nextPage?: string;

    previousPage?: string;

    size?: number;

    take?: number;

    skip?: number;

}

export class APIResponse {

    public response: string;

    public data: any;

    public pagination?: IPagination = null;

    public code: number = 0;

    public static fromResponse (response: Response) {
        const apiResponse = new APIResponse();
        const responseJson = response.json();

        for (let key in responseJson) {
            apiResponse[key] = responseJson[key];
        }

        return apiResponse;
    }

}

import {Model} from "../models/model";
export interface Storage {

    save(key: string, value: any);

    get(key: string);

    has(key: string);

    remove(key: string);

}

import {Storage} from "./storage";
import {Model} from "../models/model";
export class LocalStorage implements Storage {

    save(key: string, value: any) {
        value = value instanceof Model ? JSON.stringify(value.toObject()) : value;
        localStorage.setItem(key, value);
    }

    get(key: string) {
        return localStorage.getItem(key);
    }

    has(key: string) {
        return typeof localStorage[key] !== "undefined";
    }

    remove(key: string) {
        localStorage.removeItem(key);
    }

}

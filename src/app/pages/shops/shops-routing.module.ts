import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ShopsIndexComponent} from "./index/index.component";
import {AuthGuard} from "../../auth.guard";
import {ShopsShowComponent} from "./show/show.component";
import {ShopInformationComponent} from "./show/information/information.component";
import {ShopProductsComponent} from "./show/products/products.component";
import {ShopOptionsComponent} from "./show/options/options.component";
import {CreateShopComponent} from "./create/create.component";
import {ShopCategoriesComponent} from "./show/categories/categories.component";
import {OrdersComponent} from "./show/orders/orders.component";
import {ShopAddressesComponent} from "./addresses/addresses.component";
import {PromoCodesComponent} from "./show/promo-codes/promo-codes.component";
import {CreatePromoCodeComponent} from "./show/promo-codes/create/create.component";
import {LabelsComponent} from "./show/labels/labels.component";
import {CreateLabelComponent} from "./show/create-label/create-label.component";

const routes: Routes = [
  {
    path: 'shops',
    component: ShopsIndexComponent,
    children: [ ],
    canActivate: [ AuthGuard ],
    canActivateChild: [ AuthGuard ]
  },
  {
    path: 'shops/create',
    component: CreateShopComponent
  },
  {
    path: 'shops/:id',
    component: ShopsShowComponent,
    canActivate: [ AuthGuard ],
    canActivateChild: [ AuthGuard ],
    children: [
      {
        path: '',
        component: ShopInformationComponent
      },
      {
        path: 'information',
        component: ShopInformationComponent,
      },
      {
        path: 'products',
        component: ShopProductsComponent
      },
      {
        path: 'options',
        component: ShopOptionsComponent
      },
      {
        path: 'orders',
        component: OrdersComponent
      },
      {
        path: 'categories',
        component: ShopCategoriesComponent
      },
      {
        path: 'addresses',
        component: ShopAddressesComponent
      },
      {
        path: 'promo-codes',
        component: PromoCodesComponent
      },
      {
        path: 'promo-codes/create',
        component: CreatePromoCodeComponent
      },
      {
        path: 'labels',
        component: LabelsComponent
      },
      {
        path: 'labels/create',
        component: CreateLabelComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class ShopsRoutingModule { }

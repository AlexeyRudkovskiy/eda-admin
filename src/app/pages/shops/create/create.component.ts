import { Component, OnInit } from '@angular/core';
import {Shop} from "../../../models/shop";
import {ShopService} from "../../../services/shop.service";
import {Price} from "../../../models/price";
import {Router} from "@angular/router";

@Component({
  selector: 'app-shop-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateShopComponent implements OnInit {

  public name: string = null;

  public deliveryPrice: Price = new Price;

  public deliveryTime: number = 0;

  public minimalOrderPrice: Price = new Price;

  public minimalFreeDeliveryPrice: Price = new Price;

  private photo: File = null;

  constructor(
    private shopService: ShopService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  public createShop() {
    let createShopArgs = {
      name: this.name,
      deliveryDiscount: this.deliveryPrice.discount,
      deliveryCurrency: this.deliveryPrice.currency,
      deliveryPrice: this.deliveryPrice.amount,
      deliveryTime: this.deliveryTime,
      minimalOrderPrice: this.minimalOrderPrice,
      minimalFreeDeliveryPrice: this.minimalFreeDeliveryPrice,
      shopImage: this.photo
    };
    this.shopService.create(createShopArgs)
      .subscribe(shop => this.router.navigate([ '/shops', shop.id ]));
  }

  public photoWasUploaded(photo) {
    this.photo = photo;
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShopsIndexComponent } from './index/index.component';
import {
  MdButton, MdButtonModule, MdCheckboxModule, MdInputModule, MdProgressSpinnerModule, MdSelectModule,
  MdTableModule
} from "@angular/material";
import { ShopsShowComponent } from './show/show.component';
import {RouterModule} from "@angular/router";
import {TabsComponent} from "../../ui/tabs/tabs.component";
import {UiModule} from "../../ui/ui.module";
import { ShopInformationComponent } from './show/information/information.component';
import { ShopProductsComponent } from './show/products/products.component';
import { ShopOptionsComponent } from './show/options/options.component';
import {FormsModule} from "@angular/forms";
import { CreateShopComponent } from './create/create.component';
import { ShopCategoriesComponent } from './show/categories/categories.component';
import { OrdersComponent } from './show/orders/orders.component';
import { ShopAddressesComponent } from './addresses/addresses.component';
import {PromoCodesComponent} from "./show/promo-codes/promo-codes.component";
import { CreatePromoCodeComponent } from './show/promo-codes/create/create.component';
import { LabelsComponent } from './show/labels/labels.component';
import { CreateLabelComponent } from './show/create-label/create-label.component';

@NgModule({
  imports: [
    FormsModule,
    RouterModule,

    UiModule,

    MdInputModule,
    MdProgressSpinnerModule,
    MdButtonModule,
    MdSelectModule,
    MdCheckboxModule,
    CommonModule,
  ],
  exports: [
    MdTableModule,
  ],
  declarations: [
    ShopsIndexComponent,
    ShopsShowComponent,
    ShopInformationComponent,
    ShopProductsComponent,
    ShopOptionsComponent,
    CreateShopComponent,
    ShopCategoriesComponent,
    OrdersComponent,
    ShopAddressesComponent,
    PromoCodesComponent,
    CreatePromoCodeComponent,
    LabelsComponent,
    CreateLabelComponent
  ]
})
export class ShopsModule { }

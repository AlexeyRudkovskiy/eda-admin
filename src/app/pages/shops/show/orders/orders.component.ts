import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ShopService} from "../../../../services/shop.service";
import {BaseComponent} from "../../../../base-component";
import {Shop} from "../../../../models/shop";
import {OrderService} from "../../../../services/order.service";
import {Order} from "../../../../models/order";
import {ShowContentComponentInterface} from "../show-content-component-interface";

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'],
  providers: [ OrderService ]
})
export class OrdersComponent extends BaseComponent implements OnInit, ShowContentComponentInterface {

  public shop: Shop = null;

  public orders: Order[] = [];

  constructor(
    private shopService: ShopService,
    private orderService: OrderService,
    private activatedRouter: ActivatedRoute,
    private router: Router
  ) {
    super();
  }

  ngOnInit() {
    let paramsSubscription = this.activatedRouter.parent.params
      .pluck('id')
      .map(id => Number(id))
      .switchMap(id => this.shopService.getShop(id))
      .do(shop => this.shop = shop)
      .switchMap(shop => this.orderService.getShopOrders(shop.id))
      .subscribe(order => this.orders.push(order));

    this.registerSubscription(paramsSubscription);
  }

  public showOrder(order) {
    this.router.navigate([ '/shops', this.shop.id, 'orders', order.id ]);
  }

  public getCurrentStatus(order: Order) {
    if (order.received !== null && order.received.done !== false) {
      return 'received';
    }
    if (order.sent !== null && order.sent.done !== false) {
      return 'sent';
    }
    if (order.processing !== null && order.processing.done !== false) {
      return 'processing';
    }
    if (order.registered !== null && order.registered.done !== false) {
      return 'registered';
    }
  }

  getName(): string {
    return "orders";
  }

}

import {Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import {UiService} from "../../../ui.service";
import {TabItem} from "../../../ui/tabs/item/item.component";
import {TabsService} from "../../../services/ui/tabs.service";
import {ShopService} from "../../../services/shop.service";
import {Shop} from "../../../models/shop";
import {ActivatedRoute, Router, RouterOutlet, UrlSegment} from "@angular/router";
import {BehaviorSubject} from "rxjs/BehaviorSubject";

import 'rxjs/add/operator/take'
import 'rxjs/add/operator/defaultIfEmpty'
import {Observable} from "rxjs/Observable";
import {TabsComponent} from "../../../ui/tabs/tabs.component";
import {ShowContentComponentInterface} from "./show-content-component-interface";
import {TranslationService} from "../../../services/translation.service";

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.scss'],
  providers: [ ShopService ]
})
export class ShopsShowComponent implements OnInit {

  public tabs: TabItem[] = [];
  public shop: Shop = null;

  private informationTab: TabItem = { text: "app.shop.tabs.information", badge: 0, link: null, active: false, relativeUrl: 'information' };
  private productsTab: TabItem = { text: "app.shop.tabs.products", badge: 0, link: null, active: false, relativeUrl: 'products' };
  private optionsTab: TabItem = { text: "app.shop.tabs.options", badge: 0, link: null, active: false, relativeUrl: 'options' };
  private ordersTab: TabItem = { text: "app.shop.tabs.orders", badge: 0, link: null, active: false, relativeUrl: 'orders' };
  private categoriesTab: TabItem = { text: "app.shop.tabs.categories", badge: 0, link: null, active: false, relativeUrl: 'categories' };
  private addressesTab: TabItem = { text: "app.shop.tabs.addresses", badge: 0, link: null, active: false, relativeUrl: 'addresses' };
  private promoCodesTab: TabItem = { text: "app.shop.tabs.promo_codes", badge: 0, link: null, active: false, relativeUrl: 'promo-codes' };
  private labelsTab: TabItem = { text: "app.shop.tabs.labels", badge: 0, link: null, active: false, relativeUrl: 'labels' };

  private tabItems: TabItem[] = [];
  @ViewChild('appUiTabs') private uiTabs: TabsComponent;

  @ViewChild('routerOutlet') private routerOutlet: RouterOutlet;

  constructor(
      private ui:UiService,
      private tabsService: TabsService,
      private shopService: ShopService,
      private translationService: TranslationService,
      private activatedRouter: ActivatedRoute,
      private router: Router
  ) { }

  ngOnInit() {
    this.activatedRouter.params
      .pluck('id')
      .map(id => Number(id))
      .switchMap(id => this.shopService.getShop(id))
      .do(shop => this.proceedTabs(shop))
      .subscribe(shop => this.shop = shop);

    this.tabItems.push(this.informationTab);
    this.tabItems.push(this.productsTab);
    this.tabItems.push(this.optionsTab);
    this.tabItems.push(this.ordersTab);
    this.tabItems.push(this.categoriesTab);
    this.tabItems.push(this.addressesTab);
    // this.tabItems.push(this.promoCodesTab);
    this.tabItems.push(this.labelsTab);

    this.proceedTabs(null);

    this.ui.setHaveContentAreaPaddings(true);
    this.tabsService.addTabsItems(this.tabItems);

    this.setActiveTab('information');
    let initialUrl = this.router.url.split('/');
    initialUrl.shift();
    initialUrl.shift();
    initialUrl.shift();
    const currentComponent = initialUrl.shift();

    this.setActiveTab(currentComponent);
  }

  ngOnDestroy() {
    this.ui.setHaveContentAreaPaddings(false);
    this.tabsService.clearTabs();
  }

  public proceedTabs(shop) {
    let shopId = shop != null ? shop.id : -1;

    this.processTabItem(shopId, this.informationTab);
    this.processTabItem(shopId, this.ordersTab);
    this.processTabItem(shopId, this.optionsTab);
    this.processTabItem(shopId, this.productsTab);
    this.processTabItem(shopId, this.categoriesTab);
    this.processTabItem(shopId, this.addressesTab);
    this.processTabItem(shopId, this.promoCodesTab);
    this.processTabItem(shopId, this.labelsTab);
  }

  public tabItemClicked(tabItem: TabItem) {
    this.tabItems.forEach(item => item.active = item == tabItem);
  }

  public parseParams(params) {
    this.shopService.getShop(params.id)
        .do(this.proceedTabs.bind(this))
        .subscribe(shop => this.shop = shop);
  }

  private setActiveTab(currentItem: string) {
    this.tabItems.forEach(tabItem => tabItem.active = currentItem === tabItem.relativeUrl);
  }

  private processTabItem(shopId: number, tabItem: TabItem) {
    tabItem.text = this.translationService.translate(tabItem.text);
    if (shopId > 0) {
      tabItem.link = this.buildUrlForTabItem(shopId, tabItem);
    }
  }

  private buildUrlForTabItem(shopId: number, tabItem: TabItem) {
    return `/shops/${shopId}/${tabItem.relativeUrl}`;
  }

  onActivate($event: ShowContentComponentInterface) {
    const contentComponentName = $event.getName();
    this.setActiveTab(contentComponentName);
  }

}

import { Component, OnInit } from '@angular/core';
import {ShopService} from "../../../../services/shop.service";
import {Shop} from "../../../../models/shop";
import {ProductService} from "../../../../services/product.service";

import 'rxjs/add/operator/mergeMap'
import {ActivatedRoute} from "@angular/router";
import {Product} from "../../../../models/product";
import {BaseComponent} from "../../../../base-component";
import {ShowContentComponentInterface} from "../show-content-component-interface";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
  providers: [ ProductService ]
})
export class ShopProductsComponent extends BaseComponent implements OnInit, ShowContentComponentInterface {

  public hasProducts: boolean = false;

  public isLoading: boolean = false;

  public products: Product[] = [];

  public shopId: number = 0;

  constructor(
      private shopService: ShopService,
      private productService: ProductService,
      private activatedRoute: ActivatedRoute
  ) {
    super();
  }

  ngOnInit() {
    let loadedEventSubscription = null;
    let currentShopSubscription = null;

    loadedEventSubscription = this.productService.loadedEvent
      .subscribe(response => this.hasProducts = response.data.length === this.productService.take);

    currentShopSubscription = this.shopService.getCurrent()
      .do(shop => this.shopId = shop.id)
      .subscribe(() => this.loadProducts());

    this.registerSubscription(loadedEventSubscription);
    this.registerSubscription(currentShopSubscription);
  }

  public loadProducts() {
    let loadingProductObservable = null;

    this.isLoading = true;
    loadingProductObservable = this.productService.getShopProducts(this.shopId)
      .do(() => this.isLoading = false)
      .subscribe(product => this.products.push(product));

    this.registerSubscription(loadingProductObservable);
  }

  public deleteProduct(product: Product) {
    let deletingProductSubscription = null;

    deletingProductSubscription = this.productService.delete(this.shopId, product.id)
      .subscribe(response => this.products.splice(this.products.indexOf(product), 1));

    this.registerSubscription(deletingProductSubscription);
  }

  getName(): string {
    return "products";
  }

}

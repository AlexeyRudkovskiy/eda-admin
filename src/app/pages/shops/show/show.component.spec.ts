import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopsShowComponent } from './show.component';

describe('ShopsShowComponent', () => {
  let component: ShopsShowComponent;
  let fixture: ComponentFixture<ShopsShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopsShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopsShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

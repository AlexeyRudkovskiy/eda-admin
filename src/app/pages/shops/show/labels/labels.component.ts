import { Component, OnInit } from '@angular/core';
import {BaseComponent} from "../../../../base-component";
import {LabelsService} from "../../../../services/labels.service";
import {ActivatedRoute} from "@angular/router";
import {Label} from "../../../../models/label";
import {ShowContentComponentInterface} from "../show-content-component-interface";

@Component({
  selector: 'app-labels',
  templateUrl: './labels.component.html',
  styleUrls: ['./labels.component.scss'],
  providers: [ LabelsService ]
})
export class LabelsComponent extends BaseComponent implements OnInit, ShowContentComponentInterface {

  public labels: Label[] = [];

  private shopId: number = -1;

  constructor(
    private labelsService: LabelsService,

    private activatedRoute: ActivatedRoute
  ) {
    super();
  }

  ngOnInit() {
    this.activatedRoute.parent.params
      .pluck('id')
      .map(id => Number(id))
      .filter(id => id > 0 && !isNaN(id))
      .do(id => this.shopId = id)
      .switchMap(id => this.labelsService.getAllShopLabels(id))
      .subscribe(label => this.labels.push(label));
  }

  public deleteLabel(label: Label) {
    const index = this.labels.indexOf(label);

    this.labelsService.delete(this.shopId, label)
      .subscribe(() => this.labels.splice(index, 1));
  }

  getName(): string {
    return "labels";
  }

}

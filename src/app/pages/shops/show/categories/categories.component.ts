import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {CategoryService} from "../../../../services/category.service";
import {BaseComponent} from "../../../../base-component";
import {Category} from "../../../../models/category";
import {MdSnackBar} from "@angular/material";
import {ShowContentComponentInterface} from "../show-content-component-interface";

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
  providers: [ CategoryService ]
})
export class ShopCategoriesComponent extends BaseComponent implements OnInit, ShowContentComponentInterface {

  public isLoading: boolean = false;

  public hasMore: boolean = false;

  public categories: Category[] = [];

  public sharedCategories: Category[] = [];

  public shopId: number = 0;

  constructor(
    private activatedRouter: ActivatedRoute,
    private categoryService: CategoryService,
    private snackBar: MdSnackBar
  ) {
    super();
  }

  ngOnInit() {
    let categoriesLoadedSubscription = this.categoryService.loadedEvent
      .do(response => this.hasMore = !(response.pagination.size < response.pagination.take))
      .subscribe(response => this.isLoading = false);

    window.setTimeout(() => this.loadSharedCategories(), 1);

    let paramsSubscription = this.activatedRouter.parent.params
      .pluck('id')
      .map(id => Number(id))
      .do(id => this.isLoading = true)
      .do(id => this.shopId = id)
      .switchMap(shopId => this.categoryService.getShopCategories(shopId))
      .subscribe(category => this.categories.push(category));

    this.registerSubscription(paramsSubscription);
    this.registerSubscription(categoriesLoadedSubscription);
  }

  public loadCategories() {
    this.categoryService.getShopCategories(this.shopId)
      .subscribe(category => this.categories.push(category));
  }

  public deleteCategory(category: Category) {
    this.categoryService.delete(this.shopId, category)
      .finally(() => this.notifyCategoryDeleted(category))
      .subscribe(response => this.categories.splice(this.categories.indexOf(category), 1));
  }

  public unuseCategory(category: Category) {
    this.categoryService.unuseCategory(this.shopId, category)
      .subscribe(data => category.using = false);
  }

  public useCategory(category: Category) {
    this.categoryService.useCategory(this.shopId, category)
      .subscribe(data => category.using = true);
  }

  private notifyCategoryDeleted(category: Category) {
    this.snackBar.open(`Категория '${category.name}' успешно удалена`, null, {
      duration: 1000
    });
  }

  getName(): string {
    return "categories";
  }

  private loadSharedCategories() {
    let loadSharedCategoriesSubscription = this.categoryService.getSharedCategories(this.shopId)
      .subscribe(category => this.sharedCategories.push(category));

    this.registerSubscription(loadSharedCategoriesSubscription);
  }

}

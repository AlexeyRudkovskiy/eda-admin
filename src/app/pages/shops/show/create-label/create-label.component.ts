import { Component, OnInit } from '@angular/core';
import {BaseComponent} from "../../../../base-component";
import {ShowContentComponentInterface} from "../show-content-component-interface";
import {LabelsService} from "../../../../services/labels.service";
import {Label} from "../../../../models/label";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-create-label',
  templateUrl: './create-label.component.html',
  styleUrls: ['./create-label.component.scss'],
  providers: [ LabelsService ]
})
export class CreateLabelComponent extends BaseComponent implements OnInit, ShowContentComponentInterface {

  public isSpinnerVisible: boolean = false;

  public label: Label = new Label();

  private shopId: number = -1;

  constructor(
    private labelsService: LabelsService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    super();
  }

  ngOnInit() {
    this.activatedRoute.parent.params
      .pluck('id')
      .map(id => Number(id))
      .filter(id => id > 0 && !isNaN(id))
      .subscribe(id => this.shopId = id);
  }

  public save() {
    this.isSpinnerVisible = true;
    this.labelsService.create(this.shopId, this.label)
      .finally(() => this.navigateToLabelsList())
      .subscribe(() => this.isSpinnerVisible = false);
  }

  getName(): string {
    return "labels";
  }

  private navigateToLabelsList() {
    let currentLocation: any = location.href.split('/');
    currentLocation.pop();
    currentLocation = currentLocation.join('/');
    this.router.navigateByUrl(currentLocation);
  }

}

import { Component, OnInit } from '@angular/core';
import {MdSnackBar} from "@angular/material";
import {ActivatedRoute, Router} from "@angular/router";
import {PromoCodesService} from "../../../../../services/promo-codes.service";
import {PromoCode} from "../../../../../models/promo-code";
import {Shop} from "../../../../../models/shop";
import {ShopService} from "../../../../../services/shop.service";
import {BaseComponent} from "../../../../../base-component";
import {ShowContentComponentInterface} from "../../show-content-component-interface";

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  providers: [ PromoCodesService ]
})
export class CreatePromoCodeComponent extends BaseComponent implements OnInit, ShowContentComponentInterface {

  public promoCode: PromoCode = new PromoCode();

  public spinnerVisible: boolean = false;

  private shopId: number = -1;

  constructor(
    private shopService: ShopService,
    private promoCodeService: PromoCodesService,
    private router: Router,
    private snackBar: MdSnackBar,
    private activatedRoute: ActivatedRoute
  ) {
    super();
  }

  ngOnInit() {
    let getShopIdSubscription = this.activatedRoute.parent.params
      .pluck('id')
      .map(id => Number(id))
      .filter(id => id > 0 && !isNaN(id))
      .subscribe(item => this.processShopId(item));


    this.registerSubscription(getShopIdSubscription);
  }

  public dateChanged(date) {
    this.promoCode.expire_at = date;
  }

  public saveButtonClicked() {
    if (this.promoCode.name.length < 1 ||this.promoCode.discount <= 0) {
      this.snackBar.open(`All fields are required`, null, {
        duration: 1000
      });
      return;
    }

    this.spinnerVisible = true;
    this.promoCodeService.create(this.promoCode)
      .finally(() => this.promoCode = new PromoCode())
      .subscribe(() => this.router.navigateByUrl(`/shops/${this.shopId}/promo-codes`));
  }

  getName(): string {
    return "promo-codes";
  }

  private processShopId(shopId: number) {
    this.promoCode.shopId = shopId;
    this.shopId = shopId;
  }

}

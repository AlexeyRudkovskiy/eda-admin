import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePromoCodeComponent } from './create.component';

describe('CreatePromoCodeComponent', () => {
  let component: CreatePromoCodeComponent;
  let fixture: ComponentFixture<CreatePromoCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePromoCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePromoCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

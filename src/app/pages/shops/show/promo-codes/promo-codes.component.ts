import { Component, OnInit } from '@angular/core';
import {BaseComponent} from "../../../../base-component";
import {ShowContentComponentInterface} from "../show-content-component-interface";
import {PromoCodesService} from "../../../../services/promo-codes.service";
import {ActivatedRoute} from "@angular/router";
import {PromoCode} from "../../../../models/promo-code";
import {APIResponse} from "../../../../apiresponse";

@Component({
  selector: 'app-promo-codes',
  templateUrl: './promo-codes.component.html',
  styleUrls: ['./promo-codes.component.scss'],
  providers: [
      PromoCodesService
  ]
})
export class PromoCodesComponent extends BaseComponent implements OnInit, ShowContentComponentInterface {

  public promoCodes: PromoCode[] = [];

  public isLoading: boolean = false;

  public hasMorePromoCodes: boolean = false;

  private shopId: number = -1;

  private skip: number = 0;

  private take: number = 15;

  constructor(
      private promoCodesService: PromoCodesService,
      private activatedRoute: ActivatedRoute
  ) {
      super();
  }

  ngOnInit() {
    this.promoCodesService.promoCodesLoaded
      .subscribe(response => this.processResponse(response));

    let getShopPromoCodesSubscription = this.activatedRoute.parent.params
      .pluck('id')
      .map(id => Number(id))
      .filter(id => id > 0 && !isNaN(id))
      .do(id => this.processShopChanged(id))
      .switchMap(id => this.promoCodesService.getShopPromoCodes(id, this.take, this.skip))
      .subscribe(promoCode => this.promoCodes.push(promoCode));

    this.registerSubscription(getShopPromoCodesSubscription);
  }

  public loadPromoCodes() {
    let getShopPromoCodesSubscription = this.promoCodesService.getShopPromoCodes(this.shopId, this.take, this.skip)
      .subscribe(promoCode => this.promoCodes.push(promoCode));

    this.registerSubscription(getShopPromoCodesSubscription);
  }

  getName(): string {
      return "promo-codes";
  }

  public delete(promoCode: PromoCode) {
    let index = this.promoCodes.indexOf(promoCode);

    let deleteSubscription = this.promoCodesService.delete(promoCode)
      .finally(() => this.promoCodes.splice(index, 1))
      .subscribe(null);

    this.registerSubscription(deleteSubscription);
  }

  private processResponse(response: APIResponse) {
    let responseDataLength = response.data.length;
    this.skip += responseDataLength;
    this.hasMorePromoCodes = responseDataLength >= response.pagination.take;
  }

  private processShopChanged(id: number) {
    this.shopId = id;
    this.skip = 0;
  }
}

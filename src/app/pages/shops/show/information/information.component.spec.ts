import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopInformationComponent } from './information.component';

describe('ShopInformationComponent', () => {
  let component: ShopInformationComponent;
  let fixture: ComponentFixture<ShopInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

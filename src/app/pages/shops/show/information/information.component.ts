import { Component, OnInit, OnDestroy } from '@angular/core';
import {ShopService} from "../../../../services/shop.service";
import {Shop} from "../../../../models/shop";
import {ActivatedRoute, Params} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {Subscription} from "rxjs/Subscription";
import {MdSnackBar} from "@angular/material";
import {Location} from "@angular/common";
import {ShowContentComponentInterface} from "../show-content-component-interface";
import {LabelsService} from "../../../../services/labels.service";

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.scss'],
  providers: [ LabelsService ]
})
export class ShopInformationComponent implements OnInit, OnDestroy, ShowContentComponentInterface {

  getName(): string {
    return "information";
  }

  public shop: Shop = new Shop;

  public paramsObservable: Subscription = null;

  constructor(
      private shopService: ShopService,
      private activatedRoute: ActivatedRoute,
      private snackBar: MdSnackBar,
      private location: Location,
      private labelsService: LabelsService
  ) { }

  ngOnInit() {
    if (this.paramsObservable !== null) {
      this.paramsObservable.unsubscribe();
    }

    this.paramsObservable = this.activatedRoute.parent.params
        .pluck('id')
        .map(id => Number(id))
        .switchMap(id => this.shopService.getShop(id))
        .map(shop => shop.clone<Shop>())
        .subscribe(shop => this.shop = shop);
  }

  ngOnDestroy() {
    this.paramsObservable.unsubscribe();
  }

  public save() {
    let updateShopArgs = {
      name: this.shop.shopName,
      deliveryDiscount: this.shop.deliveryPrice.discount,
      deliveryCurrency: this.shop.deliveryPrice.currency,
      deliveryPrice: this.shop.deliveryPrice.amount,
      deliveryTime: this.shop.deliveryTime,
      minimalOrderPrice: this.shop.minimalOrderPrice,
      minimalFreeDeliveryPrice: this.shop.minimalFreeDeliveryPrice
    };

    const currentLabelInformation = this.shop.label;

    this.shopService.update(this.shop.id, updateShopArgs)
      .do(shop => shop.label = this.shop.label)
      .finally(() => this.processLabel())
      .subscribe(shop => this.shop = shop, null);
  }

  private notifyShopUpdated() {
    // this.snackBar.open('Магазин успешно обновлён', null, { duration: 1000 });
    location.reload();
  }

  public shopPosterUploaded(data: any) {
    // empty
  }

  private processLabel() {
    if (
      this.shop.label.name !== null &&
      this.shop.label.text !== null &&
      this.shop.label.name.length > 0 &&
      this.shop.label.text.length > 0
    ) {
      this.labelsService.create(this.shop.id, this.shop.label)
        .finally(() => { this.notifyShopUpdated() })
        .subscribe((labeled:any) => this.shop.label = labeled.label);
    } else {
      this.notifyShopUpdated();
    }
  }

}

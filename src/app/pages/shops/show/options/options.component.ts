import { Component, OnInit, OnDestroy } from '@angular/core';
import {Option} from "../../../../models/option";
import {ActivatedRoute} from "@angular/router";
import {OptionService} from "../../../../services/option.service";
import {Subscription} from "rxjs/Subscription";
import {BaseComponent} from "../../../../base-component";
import {ShowContentComponentInterface} from "../show-content-component-interface";

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.scss']
})
export class ShopOptionsComponent extends BaseComponent implements OnInit, OnDestroy, ShowContentComponentInterface {

  public options: Option[] = [];

  public isLoading: boolean = false;

  public isLoadedAllRecords: boolean = false;

  private shopId: number = -1;

  constructor(
    private activatedRoute: ActivatedRoute,
    private optionService: OptionService
  ) {
    super();
  }

  ngOnInit() {
    this.isLoading = true;
    this.optionService.skip = 0;

    let onLoadedSubscription = null;
    let paramsSubscriptions = null;

    onLoadedSubscription = this.optionService.onLoaded.subscribe(data => {
      this.isLoadedAllRecords = data.pagination.size < data.pagination.take;
    });

    paramsSubscriptions = this.activatedRoute.parent.params
      .pluck('id')
      .map(shopId => Number(shopId))
      .do(shopId => this.shopId = shopId)
      .switchMap(shopId => this.optionService.getShopOptions(shopId))
      .do(option => this.isLoading = false)
      .subscribe(option => this.options.push(option), null, () => this.isLoading = false);

    this.registerSubscription(onLoadedSubscription);
    this.registerSubscription(paramsSubscriptions);
  }

  public loadOptions() {
    this.isLoading = true;

    this.optionService.getShopOptions(this.shopId)
      .finally(() => this.isLoading = false)
      .subscribe(option => this.options.push(option));
  }

  public deleteOption(option: Option) {
    this.optionService.delete(this.shopId, option)
      .subscribe(response => this.options.splice(this.options.indexOf(option), 1));
  }

  getName(): string {
    return "options";
  }

}

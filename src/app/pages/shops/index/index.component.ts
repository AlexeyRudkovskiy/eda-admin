import { Component, OnInit, OnDestroy } from '@angular/core';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import {ShopService} from "../../../services/shop.service";
import {Shop} from "../../../models/shop";

import 'rxjs/add/operator/finally'
import {APIResponse} from "../../../apiresponse";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers: [ ShopService ]
})
export class ShopsIndexComponent implements OnInit {

  public dataSource: Shop[] = [];

  public isLoading: boolean = false;

  public isLoadedAllRecords: boolean = false;

  constructor(private shopService: ShopService) { }

  ngOnInit() {
    this.loadShops();
    this.shopService.loadedEvent.subscribe(response => this.parseResponseData(response));
  }

  public loadShops()
  {
    this.isLoading = true;
    this.shopService
        .getList(this.dataSource.length, this.shopService.take)
        .finally((...params) => this.isLoading = false)
        .subscribe(shop => this.dataSource.push(shop));
  }

  private parseResponseData(response: APIResponse)
  {
    this.isLoadedAllRecords = response.data.length < this.shopService.take;
  }

}

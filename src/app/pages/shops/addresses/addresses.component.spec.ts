import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopAddressesComponent } from './addresses.component';

describe('ShopAddressesComponent', () => {
  let component: ShopAddressesComponent;
  let fixture: ComponentFixture<ShopAddressesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopAddressesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopAddressesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

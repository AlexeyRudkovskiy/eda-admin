import { Component, OnInit } from '@angular/core';
import {BaseComponent} from "../../../base-component";
import {ShopService} from "../../../services/shop.service";
import {AddressService} from "../../../services/address.service";
import {ActivatedRoute} from "@angular/router";
import {Address} from "../../../models/address";
import {ShowContentComponentInterface} from "../show/show-content-component-interface";

@Component({
  selector: 'app-addresses',
  templateUrl: './addresses.component.html',
  styleUrls: ['./addresses.component.scss'],
  providers: [ AddressService ]
})
export class ShopAddressesComponent extends  BaseComponent implements OnInit, ShowContentComponentInterface {

  public isLoading: boolean = true;

  public hasMoreAddresses: boolean = false;

  public addresses: Address[] = [];

  private shopId: number = -1;

  constructor(
    private addressService: AddressService,
    private activatedRoute: ActivatedRoute
  ) {
    super();
  }

  ngOnInit() {
    let loadedEventSubscription = this.addressService.loadedEvent
      .do(response => this.isLoading = false)
      .subscribe(response => this.hasMoreAddresses = response.pagination.take === response.pagination.size);

    let paramsSubscription = this.activatedRoute.parent.params.pluck('id')
      .map(id => Number(id))
      .do(id => this.shopId = id)
      .switchMap(id => this.addressService.getShopAddresses(id))
      .subscribe(address => this.addresses.push(address));

    this.registerSubscription(loadedEventSubscription);
    this.registerSubscription(paramsSubscription);
  }

  public loadAddresses() {
    this.isLoading = true;

    let loadingAddressesSubscription = this.addressService.getShopAddresses(this.shopId)
      .subscribe(address => this.addresses.push(address));
  }

  public delete(address: Address) {
    this.addressService.delete(this.shopId, address)
      .subscribe(response => this.addresses.splice(this.addresses.indexOf(address), 1));
  }

  getName(): string {
    return "addresses";
  }

}

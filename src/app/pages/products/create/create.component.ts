import { Component, OnInit, OnDestroy } from '@angular/core';
import {Product} from "../../../models/product";
import {ProductService} from "../../../services/product.service";
import {ActivatedRoute, Router} from "@angular/router";

import 'rxjs/add/operator/pluck'
import {UiService} from "../../../ui.service";
import {ShopService} from "../../../services/shop.service";
import {Shop} from "../../../models/shop";

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateProductComponent implements OnInit, OnDestroy {

  public product: Product = new Product;

  public shop: Shop = null;

  public saveSpinnerVisible: boolean = false;

  private shopId: number = -1;

  private photo: File = null;

  constructor(
      private productService: ProductService,
      private shopService: ShopService,
      private router: Router,
      private activatedRouter: ActivatedRoute,
      private ui: UiService
  ) { }

  ngOnInit() {
    this.ui.setHaveContentAreaPaddings(true);

    this.activatedRouter.params
      .pluck('shop_id')
      .map(shopId => Number(shopId))
      .switchMap(shopId => this.shopService.getShop(shopId))
      .subscribe(shop => this.shop = shop);

    this.activatedRouter.params.pluck('shop_id')
        .subscribe(value => this.shopId = Number(value));
  }

  ngOnDestroy() {
    this.ui.setHaveContentAreaPaddings(false);
  }

  public saveProduct() {
    this.saveSpinnerVisible = true;

    this.productService.create(this.shopId, this.product, this.photo)
        .do(data => this.saveSpinnerVisible = false)
        .subscribe(product => this.router.navigate([ '/shops', this.shopId, 'products', product.id ]));
  }

  public photoWasUploaded(photo) {
    this.photo = photo;
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductShowSizesComponent } from './sizes.component';

describe('ProductShowSizesComponent', () => {
  let component: ProductShowSizesComponent;
  let fixture: ComponentFixture<ProductShowSizesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductShowSizesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductShowSizesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

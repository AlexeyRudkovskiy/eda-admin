import { Component, OnInit } from '@angular/core';
import {ProductService} from "../../../../services/product.service";
import {Product} from "../../../../models/product";
import {SizeService} from "../../../../services/size.service";
import {Size} from "../../../../models/size";
import {OptionService} from "../../../../services/option.service";

@Component({
  selector: 'app-sizes',
  templateUrl: './sizes.component.html',
  styleUrls: ['./sizes.component.scss'],
  providers: [ SizeService ]
})
export class ProductShowSizesComponent implements OnInit {

  public product: Product = new Product;

  constructor(
      private productService: ProductService,
      private sizeService: SizeService
  ) { }

  ngOnInit() {
    this.productService.currentProduct
        .subscribe(product => this.product = product);
  }

  public deleteSize(size: Size) {
    this.sizeService.delete(this.product.shop.id, this.product.id, size)
      .subscribe(response => this.product.sizes.splice(this.product.sizes.indexOf(size), 1));
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsShowComponent } from './show.component';

describe('ProductsShowComponent', () => {
  let component: ProductsShowComponent;
  let fixture: ComponentFixture<ProductsShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductShowInformationComponent } from './information.component';

describe('ProductShowInformationComponent', () => {
  let component: ProductShowInformationComponent;
  let fixture: ComponentFixture<ProductShowInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductShowInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductShowInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

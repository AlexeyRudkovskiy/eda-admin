import { Component, OnInit } from '@angular/core';
import {ProductService} from "../../../../services/product.service";
import {Product} from "../../../../models/product";
import {CategoryService} from "../../../../services/category.service";
import {BaseComponent} from "../../../../base-component";
import {Category} from "../../../../models/category";
import {MdSnackBar} from "@angular/material";
import {LabelsService} from "../../../../services/labels.service";

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.scss'],
  providers: [ CategoryService, LabelsService ]
})
export class ProductShowInformationComponent extends BaseComponent implements OnInit {

  public product: Product = new Product;

  public categories: Category[] = [];

  public currentProductCategories: number[] = [];

  constructor(
      private productService: ProductService,
      private categoryService: CategoryService,
      private labelsService: LabelsService,
      private snackBar: MdSnackBar
  ) {
    super();
  }

  ngOnInit() {
    let currentProductSubscription = this.productService.currentProduct
      .map(product => product.clone<Product>())
      .finally(() => this.loadShopCategories())
      .subscribe(product => this.product = product);

    this.registerSubscription(currentProductSubscription);
  }

  public saveProduct() {
    let fieldsValues = {
      name: this.product.productName,
      price: this.product.price.toObject(),
      categories: this.categories.filter(category => category.selected).map(category => category.id),
      consist_of: this.product.consistOf
    };

    let saveSubscription = this.productService.save(fieldsValues, this.product.shop.id, this.product.id)
      .finally(() => this.processLabelCreatedOrUpdated())
      .subscribe(() => {
        /// todo: Should notify that product successfully updated
      });

    this.registerSubscription(saveSubscription);
  }

  private notifyProductUpdated() {
    this.snackBar.open('Продукт успешно обновлён', null, {
      duration: 1000
    });
  }

  private loadShopCategories() {
    this.currentProductCategories = this.product.categories.map(category => category.id);

    let allCategoriesSubscription = this.categoryService
      .getShopCategories(this.product.shop.id)
      .map(category => this.processCategory(category))
      .subscribe(category => this.categories.push(category));

    this.registerSubscription(allCategoriesSubscription);
  }

  private processCategory(category: Category): Category {
    category.selected = this.currentProductCategories.indexOf(category.id) > -1;
    return category;
  }

  private processLabelCreatedOrUpdated() {
    if (
      this.product.label.name !== null &&
      this.product.label.text !== null &&
      this.product.label.name.length > 0 &&
      this.product.label.text.length > 0
    ) {
      this.labelsService.create(this.product.shop.id, this.product.label, this.product.id)
        .finally(() => { this.notifyProductUpdated() })
        .subscribe((labeled:any) => this.product.label = labeled.label);
    } else {
      this.notifyProductUpdated();
    }
  }

}

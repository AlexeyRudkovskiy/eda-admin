import { Component, OnInit } from '@angular/core';
import {Review} from "../../../../models/review";
import {ReviewService} from "../../../../services/review.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.scss'],
  providers: [ ReviewService ]
})
export class ProductShowReviewsComponent implements OnInit {

  public hasProducts: boolean = true;

  public isLoading: boolean = true;

  public reviews: Review[] = [];

  private shopId: number = -1;

  private productId: number = -1;

  constructor(
    private reviewService: ReviewService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.parent.params
      .map(params => this.processParams(params))
      .switchMap(params => this.reviewService.getProductReviews(params.productId))
      .subscribe(review => this.reviews.push(review));

    this.isLoading = true;
    this.reviewService.loadedEvent
        .do(response => this.isLoading = false)
        .subscribe(response => this.hasProducts = response.data.length == response.pagination.take);
  }

  public loadReviews() {
    this.reviewService.getProductReviews(this.productId)
        .subscribe(review => this.reviews.push(review));
  }

  public processParams(params) {
    const shopId: number = params.shop_id;
    const productId: number = params.id;

    this.shopId = shopId;
    this.productId = productId;

    return { shopId, productId };
  }

}

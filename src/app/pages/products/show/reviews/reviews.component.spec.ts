import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductShowReviewsComponent } from './reviews.component';

describe('ProductShowReviewsComponent', () => {
  let component: ProductShowReviewsComponent;
  let fixture: ComponentFixture<ProductShowReviewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductShowReviewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductShowReviewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

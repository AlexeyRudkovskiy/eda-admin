import { Component, OnInit, OnDestroy } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ProductService} from "../../../services/product.service";
import {UiService} from "../../../ui.service";
import {TabItem} from "../../../ui/tabs/item/item.component";
import {Product} from "../../../models/product";
import {CacheService} from "../../../services/cache.service";
import {TranslationService} from "../../../services/translation.service";

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.scss'],
  providers: [ProductService, CacheService]
})
export class ProductsShowComponent implements OnInit, OnDestroy {

  private informationTab: TabItem = { link: 'information', relativeUrl: 'information', active: false, text: "app.product.tabs.information" };
  private reviewsTab: TabItem = { link: 'reviews', relativeUrl: 'reviews', active: false, text: "app.product.tabs.reviews" };
  private sizesTab: TabItem = { link: 'sizes', relativeUrl: 'sizes', active: false, text: "app.product.tabs.sizes" };

  public tabs: TabItem[] = [
      this.informationTab,
      this.reviewsTab,
      this.sizesTab
  ];

  public product: Product = null;

  constructor(
      private productService: ProductService,
      private uiService: UiService,
      private activatedRoute: ActivatedRoute,
      private cacheService: CacheService,
      private translationService: TranslationService
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => this.handleRouterParams(params));
    this.uiService.setHaveContentAreaPaddings(true);

    this.tabs.forEach(tab => tab.text = this.translationService.translate(tab.text));

    const child = this.activatedRoute.firstChild;
    if (child !== null) {
      child.url
          .map(data => data.length < 1 ? [{ path: 'information' }] : data)
          .concatMap(item => item)
          .map(item => item.path)
          .take(1)
          .subscribe(currentItem => this.setActiveTab(currentItem));
    }
  }

  ngOnDestroy() {
    this.uiService.setHaveContentAreaPaddings(false);
    this.productService.recreateCurrentProductReplaySubject();
  }

  private handleRouterParams(params) {
    const {shop_id, id} = params;

    this.productService.getProduct(shop_id, id)
        .subscribe(product => this.product = product);

    this.productService.updateProduct
        .subscribe(product => this.product = product);
  }

  private setActiveTab(item: string) {
    this.tabs.forEach(tab => tab.active = tab.relativeUrl === item);
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from "../../auth.guard";
import {ProductsShowComponent} from "./show/show.component";
import {ProductShowInformationComponent} from "./show/information/information.component";
import {ProductShowReviewsComponent} from "./show/reviews/reviews.component";
import {ProductShowSizesComponent} from "./show/sizes/sizes.component";
import {CreateProductComponent} from "./create/create.component";

const routes: Routes = [
  {
      path: 'shops/:shop_id/products/create',
      component: CreateProductComponent
  },
  {
    path: 'shops/:shop_id/products/:id',
    component: ProductsShowComponent,
    children: [
      {
        path: '',
        component: ProductShowInformationComponent
      },
      {
        path: 'information',
        component: ProductShowInformationComponent
      },
      {
        path: 'reviews',
        component: ProductShowReviewsComponent
      },
      {
        path: 'sizes',
        component: ProductShowSizesComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }

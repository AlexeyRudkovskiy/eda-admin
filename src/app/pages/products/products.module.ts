import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsShowComponent } from './show/show.component';
import {UiModule} from "../../ui/ui.module";
import {RouterModule} from "@angular/router";
import { ProductShowInformationComponent } from './show/information/information.component';
import { ProductShowReviewsComponent } from './show/reviews/reviews.component';
import {MdButtonModule, MdInputModule, MdProgressSpinnerModule} from "@angular/material";
import {FormsModule} from "@angular/forms";
import { ProductShowSizesComponent } from './show/sizes/sizes.component';
import { CreateProductComponent } from './create/create.component';

@NgModule({
  imports: [
    FormsModule,
    MdButtonModule,
    MdProgressSpinnerModule,
    MdInputModule,

    CommonModule,
    UiModule,
    RouterModule,
  ],
  declarations: [
    ProductsShowComponent,
    ProductShowInformationComponent,
    ProductShowReviewsComponent,
    ProductShowSizesComponent,
    CreateProductComponent
  ]
})
export class ProductsModule { }

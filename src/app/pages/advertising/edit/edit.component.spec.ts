import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAdvertisingComponent } from './edit.component';

describe('EditAdvertisingComponent', () => {
  let component: EditAdvertisingComponent;
  let fixture: ComponentFixture<EditAdvertisingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAdvertisingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAdvertisingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

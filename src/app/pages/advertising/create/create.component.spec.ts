import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAdvertisingComponent } from './create.component';

describe('CreateAdvertisingComponent', () => {
  let component: CreateAdvertisingComponent;
  let fixture: ComponentFixture<CreateAdvertisingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAdvertisingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAdvertisingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

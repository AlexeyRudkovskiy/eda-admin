import { Component, OnInit } from '@angular/core';
import {BaseComponent} from "../../../base-component";
import {AdvertisingService} from "../../../services/advertising.service";
import {MdSnackBar} from "@angular/material";
import {Router} from "@angular/router";
import {Shop} from "../../../models/shop";
import {ShopService} from "../../../services/shop.service";
import {CategoryService} from "../../../services/category.service";
import {Category} from "../../../models/category";
import {Product} from "../../../models/product";
import {ProductService} from "../../../services/product.service";
import {Subscription} from "rxjs/Subscription";

@Component({
  selector: 'app-create-ads',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  providers: [ AdvertisingService, CategoryService ]
})
export class CreateAdvertisingComponent extends BaseComponent implements OnInit {

  public name: string;

  public isActive: boolean;

  public advertising_title: string;

  public advertising_content: string;

  public isSpinnerVisible: boolean = false;

  public advertising_type: number = -1;

  public image: File = null;

  public advertising_types:any = [
    { name: 'Заведение', id: 1 },
    { name: 'Категория', id: 2 },
    { name: 'Продукт', id: 3 }
  ];

  public shop_id: number = -1;

  public category_id: number = -1;

  public product_id: number = -1;

  public shops: Shop[] = [  ];

  public categories: Category[] = [];

  public products: Product[] = [];

  public showCategorySelect: boolean = false;

  public showProductSelect: boolean = false;

  private lastSubscription: Subscription = null;

  constructor(
    private adsService: AdvertisingService,
    private snackBar: MdSnackBar,
    private router: Router,

    private shopService: ShopService,
    private categoryService: CategoryService,
    private productsService: ProductService
  ) {
    super();
  }

  ngOnInit() {
    this.shopService.getList(0, 0, [ 'name', 'id' ])
      .subscribe(shop => this.shops.push(shop));
  }

  public createAds() {
    this.isSpinnerVisible = true;

    let actionAttributes = {};
    let actionType = '';

    if (this.advertising_type == 1) {
      actionType = 'shop';
    } else if (this.advertising_type == 2) {
      actionType = 'shop_category';
      actionAttributes = {
        category_id: this.category_id
      };
    } else if (this.advertising_type == 3) {
      actionType = 'product';
      actionAttributes = {
        id: this.product_id
      };
    }

    if (this.shop_id > 0 && this.name.length > 0 && this.advertising_title.length > 0 && this.advertising_content.length > 0 && actionType.length > 0 && this.image !== null) {
      this.adsService.create(
        this.shop_id.toString(),
        this.name,
        this.isActive,
        this.advertising_title,
        this.advertising_content,
        actionType,
        actionAttributes,
        this.image
      ).subscribe(null, null, () => this.processAdsCreated());
    }
  }

  private processAdsCreated() {
    this.isSpinnerVisible = false;

    this.snackBar.open('Объявление успешно создано', null, { duration: 1000 });
    this.router.navigate([ '/advertising' ]);
  }

  public imageUploaded(image) {
    this.image = image;
  }

  public advertisingTypeChanged(type) {
    this.advertising_type = type.value;
    this.updateSelectVisibility();
  }

  public shopChanged(shop) {
    this.shop_id = shop.value;
    this.updateSelectVisibility();
  }

  public categoryChanged(category) {
    this.category_id = category.value;
  }

  public productChanged(product) {
    this.product_id = product.value;
  }

  private updateSelectVisibility() {
    this.showCategorySelect = this.advertising_type === 2 && this.shop_id > -1;
    this.showProductSelect  = this.advertising_type === 3 && this.shop_id > -1;

    if (this.lastSubscription !== null) {
      this.lastSubscription.unsubscribe();
    }

    if (this.showCategorySelect) {

      if (this.categories.length > 0) {
        this.categories.splice(0, this.categories.length);
      }
      this.category_id = -1;
      this.lastSubscription = this.categoryService.getShopCategories(this.shop_id, 99999, 0)
        .subscribe(category => this.categories.push(category));
    }

    if (this.showProductSelect) {

      if (this.products.length > 0) {
        this.products.splice(0, this.products.length);
      }
      this.product_id = -1;
      this.lastSubscription = this.productsService.getShopProducts(this.shop_id, 999999, 0)
        .do(product => console.log(product))
        .subscribe(product => this.products.push(product));
    }
  }

}

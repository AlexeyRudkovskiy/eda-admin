import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertisingIndexComponent } from './index.component';

describe('AdvertisingIndexComponent', () => {
  let component: AdvertisingIndexComponent;
  let fixture: ComponentFixture<AdvertisingIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertisingIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertisingIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

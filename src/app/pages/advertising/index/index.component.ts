import { Component, OnInit } from '@angular/core';
import {AdvertisingService} from "../../../services/advertising.service";
import {Advertising} from "../../../models/ads";
import {BaseComponent} from "../../../base-component";
import {MdSnackBar} from "@angular/material";

@Component({
  selector: 'app-advertising-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers: [ AdvertisingService ]
})
export class AdvertisingIndexComponent extends BaseComponent implements OnInit {

  public advertisings: Advertising[] = [];

  public isLoading: boolean = false;

  public hasMoreAdvertisings: boolean = true;

  constructor(
    private advertisingService: AdvertisingService,
    private snackBar: MdSnackBar
  ) {
    super();
  }

  ngOnInit() {
    let loadedEventSubscription = this.advertisingService.loadedEvent
      .subscribe(response => this.hasMoreAdvertisings = response.pagination.size == response.pagination.take);

    let getAdsSubscription = this.advertisingService.getAdsList()
      .subscribe(ads => this.advertisings.push(ads));

    this.registerSubscription(loadedEventSubscription);
    this.registerSubscription(getAdsSubscription);
  }

  public loadAdvertisings() {
    let getAdsSubscription = this.advertisingService.getAdsList()
      .subscribe(ads => this.advertisings.push(ads));
    this.registerSubscription(getAdsSubscription);
  }

  public delete(ads: Advertising) {
    let deleteSubscription = this.advertisingService.delete(ads)
      .subscribe(null, null, () => this.processAdsDeleted(ads));

    this.registerSubscription(deleteSubscription);
  }

  private processAdsDeleted(ads: Advertising) {
    this.snackBar.open('Объявление успешно удалено', null, { duration: 1000 });
    this.advertisings.splice(this.advertisings.indexOf(ads), 1);
  }

}

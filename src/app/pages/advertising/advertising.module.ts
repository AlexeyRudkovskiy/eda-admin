import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdvertisingIndexComponent } from './index/index.component';
import {FormsModule} from "@angular/forms";
import {
  MdButtonModule, MdCheckboxModule, MdInputModule, MdProgressSpinnerModule,
  MdSelectModule
} from "@angular/material";
import { CreateAdvertisingComponent } from './create/create.component';
import {RouterModule} from "@angular/router";
import {UiModule} from "../../ui/ui.module";
import { EditAdvertisingComponent } from './edit/edit.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,

    UiModule,

    MdInputModule,
    MdProgressSpinnerModule,
    MdButtonModule,
    MdCheckboxModule,
    MdSelectModule,
  ],
  declarations: [AdvertisingIndexComponent, CreateAdvertisingComponent, EditAdvertisingComponent ]
})
export class AdvertisingModule { }

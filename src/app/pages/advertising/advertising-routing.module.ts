import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdvertisingIndexComponent} from "./index/index.component";
import {CreateAdvertisingComponent} from "./create/create.component";
import {EditAdvertisingComponent} from "./edit/edit.component";

const routes: Routes = [
  {
    path: 'advertising',
    component: AdvertisingIndexComponent
  },
  {
    path: 'advertising/create',
    component: CreateAdvertisingComponent
  },
  {
    path: 'advertising/:id',
    component: EditAdvertisingComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AdvertisingRoutingModule { }


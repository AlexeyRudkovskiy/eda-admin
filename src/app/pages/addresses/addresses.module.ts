import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateAddressComponent } from './create/create.component';
import {UiModule} from "../../ui/ui.module";
import {FormsModule} from "@angular/forms";
import {MdInputModule} from "@angular/material";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    MdInputModule,

    UiModule,
  ],
  declarations: [CreateAddressComponent]
})
export class AddressesModule { }

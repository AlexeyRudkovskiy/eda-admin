import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CreateAddressComponent} from "./create/create.component";

const routes: Routes = [
  {
    path: 'shops/:shop_id/addresses/create',
    component: CreateAddressComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AddressesRoutingModule { }

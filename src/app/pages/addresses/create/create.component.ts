import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {BaseComponent} from "../../../base-component";
import {AddressService, CreateAddressArguments} from "../../../services/address.service";
import {ShopService} from "../../../services/shop.service";
import {Shop} from "../../../models/shop";
import {UiService} from "../../../ui.service";
import {Address} from "../../../models/address";
import {Location} from "../../../models/location";
import {Location as AngularLocation} from "@angular/common"
import {MdSnackBar} from "@angular/material";

@Component({
  selector: 'app-create-address',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  providers: [ AddressService ]
})
export class CreateAddressComponent extends BaseComponent implements OnInit, OnDestroy {

  public address: Address = new Address;

  public shop: Shop = null;

  public spinnerVisible: boolean = false;

  public error: string;

  private opensAt: string;

  private closesAt: string;

  private shopId: number = -1;

  private location: Location = null;

  constructor(
    private activatedRouter: ActivatedRoute,
    private uiService: UiService,
    private addressService: AddressService,
    private shopService: ShopService,
    private angularLocation: AngularLocation,
    private snackBar: MdSnackBar
  ) {
    super();
  }

  ngOnInit() {
    this.uiService.setHaveContentAreaPaddings(true);

    const paramsSubscription = this.activatedRouter.params
      .pluck('shop_id')
      .map(shopId => Number(shopId))
      .do(shopId => this.shopId = shopId)
      .switchMap(shopId => this.shopService.getShop(shopId))
      .subscribe(shop => this.shop = shop);

    this.registerSubscription(paramsSubscription);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();

    this.uiService.setHaveContentAreaPaddings(false);
  }

  public locationWasChanged(location: Location) {
    this.location = location;
  }

  public opensAtTimeWasChanged(time: string) {
    this.opensAt = time;
  }

  public closesAtTimeWasChanged(time: string) {
    this.closesAt = time;
  }

  public createAddress() {
    if (
      this.location === null ||
      this.address.city === null ||
      this.address.district === null ||
      this.address.address.length < 1
    ) {
      this.error = 'Все поля должны быть заполнены';
      document.querySelector('.content-wrapper').scrollTop = 0;
      return;
    }

    this.error = null;

    const createAddressArgs: CreateAddressArguments = {
      location: this.location,
      district: this.address.district,
      address: this.address.address,
      opensFrom: this.opensAt,
      closesAt: this.closesAt
    };

    this.addressService.create(this.shopId, createAddressArgs)
      .subscribe(address => this.angularLocation.back(), null, () => this.processAddressCreated());
  }

  private processAddressCreated() {
    this.snackBar.open('Адрес успешно создан', null, { duration: 1000 });
  }

}

import { Component, OnInit } from '@angular/core';
import {TranslationService} from "../../../services/translation.service";
import {TranslationDirective} from "../../../ui/directives/translation.directive";
import {PromoCode} from "../../../models/promo-code";
import {ShopService} from "../../../services/shop.service";
import {Shop} from "../../../models/shop";
import {BaseComponent} from "../../../base-component";
import {PromoCodesService} from "../../../services/promo-codes.service";
import {Router} from "@angular/router";
import {MdSnackBar} from "@angular/material";

@Component({
  selector: 'app-create-promo-code',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  providers: [
      TranslationService,
      PromoCodesService
  ]
})
export class CreateComponent extends BaseComponent implements OnInit {

  public promoCode: PromoCode = new PromoCode();

  public shops: Shop[] = [];

  public spinnerVisible: boolean = false;

  constructor(
      private shopService: ShopService,
      private promoCodeService: PromoCodesService,
      private router: Router,
      private snackBar: MdSnackBar
  ) {
    super();
  }

  ngOnInit() {
    let getShopsListSubscription = this.shopService.getList(0, 99999, [ 'name', 'id' ])
        .subscribe(shop => this.shops.push(shop));
    this.registerSubscription(getShopsListSubscription);
  }

  public dateChanged(date) {
    this.promoCode.expire_at = date;
  }

  public saveButtonClicked() {
    if (this.promoCode.name.length < 1 ||this.promoCode.discount <= 0) {
      this.snackBar.open(`All fields are required`, null, {
        duration: 1000
      });
      return;
    }

    this.spinnerVisible = true;
    this.promoCodeService.create(this.promoCode)
        .finally(() => this.promoCode = new PromoCode())
        .subscribe(() => this.router.navigateByUrl('/promo-codes'));
  }

}

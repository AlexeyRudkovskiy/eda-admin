import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PromoCodesRoutingModule } from './promo-codes-routing.module';
import { IndexComponent } from './index/index.component';
import {
  MdButtonModule, MdCheckboxModule, MdInputModule, MdProgressSpinnerModule,
  MdSelectModule
} from "@angular/material";
import { CreateComponent } from './create/create.component';
import {FormsModule} from "@angular/forms";
import {UiModule} from "../../ui/ui.module";

@NgModule({
  imports: [
    CommonModule,
    PromoCodesRoutingModule,
    MdProgressSpinnerModule,
    MdButtonModule,
    FormsModule,
    MdInputModule,
    MdSelectModule,
    MdCheckboxModule,

    UiModule
  ],
  exports: [
    IndexComponent, CreateComponent
  ],
  declarations: [IndexComponent, CreateComponent]
})
export class PromoCodesModule { }

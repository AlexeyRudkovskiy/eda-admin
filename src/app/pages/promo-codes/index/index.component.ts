import { Component, OnInit } from '@angular/core';
import {PromoCodesService} from "../../../services/promo-codes.service";
import {BaseComponent} from "../../../base-component";
import {PromoCode} from "../../../models/promo-code";

@Component({
  selector: 'app-promo-codes-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers: [ PromoCodesService ]
})
export class IndexComponent extends BaseComponent implements OnInit{

  public promoCodes: PromoCode[] = [];

  public isLoading: boolean = false;

  public hasMoreItem: boolean = true;

  constructor(private promoCodesService: PromoCodesService) {
    super();
  }

  ngOnInit() {
    this.isLoading = true;

    let fetchAllPromoCodesSubscription = this.promoCodesService.getAllPromoCodes()
        .finally(() => this.processDataLoaded())
        .subscribe(promoCode => this.promoCodes.push(promoCode));

    let noMoreItemsSubscription = this.promoCodesService.noMoreItemsEvent
        .subscribe(() => this.hasMoreItem = false);

    this.registerSubscription(fetchAllPromoCodesSubscription);
    this.registerSubscription(noMoreItemsSubscription);
  }

  public loadPromoCodes() {
    this.isLoading = true;

    let loadPromoCodesSubscription = this.promoCodesService.getAllPromoCodes()
        .finally(() => this.processDataLoaded())
        .subscribe(promoCode => this.promoCodes.push(promoCode));

    this.registerSubscription(loadPromoCodesSubscription);
  }

  public deletePromoCode(promoCode) {
    let position = this.promoCodes.indexOf(promoCode);
    this.promoCodesService.delete(promoCode)
        .subscribe(() => this.promoCodes.splice(position, 1));
  }

  private processDataLoaded() {
    this.isLoading = false;
  }

}

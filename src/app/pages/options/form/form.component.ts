import {Component, EventEmitter, Input, OnInit, OnDestroy, Output} from '@angular/core';
import {Option} from "../../../models/option";
import {ActivatedRoute, Router} from "@angular/router";
import {OptionService} from "../../../services/option.service";
import {MdSnackBar} from '@angular/material';
import {UiService} from "../../../ui.service";
import {ShopService} from "../../../services/shop.service";
import {Shop} from "../../../models/shop";
import {TranslationService} from "../../../services/translation.service";


@Component({
  selector: 'app-options-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class OptionsFormComponent implements OnInit, OnDestroy {

  @Input() public option: Option = new Option;

  public shop: Shop = null;

  private data: any = {};

  private shopId: number = -1;

  private optionId: number = -1;

  constructor(
    private activatedRouter: ActivatedRoute,
    private router: Router,
    private optionService: OptionService,
    private shopService: ShopService,
    private snackBar: MdSnackBar,
    private ui: UiService,
    private translationService: TranslationService
  ) { }

  ngOnInit() {
    this.data = this.activatedRouter.snapshot.data;
    this.activatedRouter.params
      .do(params => this.processParams(params))
      .switchMap(params => this.shopService.getShop(params.shop_id))
      .do(shop => this.shop = shop)
      .do(shop => console.log(this.optionId > -1, this.optionId))
      .filter(shop => this.optionId > -1)
      .switchMap(shop => this.optionService.getShopOption(shop.id, this.optionId))
      .subscribe(option => this.option = option);

    // this.activatedRouter.params.forEach(params => this.processParams(params));
    this.ui.setHaveContentAreaPaddings(true);
  }

  ngOnDestroy() {
    this.ui.setHaveContentAreaPaddings(false);
  }


  private processParams(params: any) {
    this.shopId = Number(params.shop_id);

    if (typeof params.option_id !== "undefined") {
      this.optionId = Number(params.option_id);
    }

    return params;

    // if (this.optionId > 0) {
    //   this.optionService.getShopOption(this.shopId, this.optionId)
    //     .subscribe(option => this.option = option);
    // }
  }

  public saveOption() {
    if (this.optionId > 0) {
      this.optionService.update(this.shopId, this.option)
        .subscribe(option => this.optionWasUpdated());
    } else {
      this.optionService.create(this.shopId, this.option)
        .subscribe(option => this.saveCreatedOption(option));
    }
  }

  public saveCreatedOption(option: Option) {
    this.option = option;
    this.optionId = option.id;
    this.snackBar.open(this.translationService.translate('app.option.created'), null, {
      duration: 1000
    });
  }

  public optionWasUpdated() {
    this.snackBar.open(this.translationService.translate('app.option.updated'), null, {
      duration: 1000
    });
  }

}

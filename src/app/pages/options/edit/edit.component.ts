import { Component, OnInit } from '@angular/core';
import {Option} from "../../../models/option";
import {OptionService} from "../../../services/option.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditOptionComponent implements OnInit {

  public option: Option = new Option;

  constructor(
      private optionService: OptionService,
      private activatedRouter: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRouter.params.forEach(params => this.proceedParams(params));
  }

  public proceedParams(params) {
    let shopId = Number(params.shop_id);
    let optionId = Number(params.option_id);
    this.optionService.getShopOption(shopId, optionId)
        .subscribe(option => this.option = option);
  }

  public saveOption() {
  }

}

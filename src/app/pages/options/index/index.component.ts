import { Component, OnInit, OnDestroy } from '@angular/core';
import {ProductService} from "../../../services/product.service";
import {ShopService} from "../../../services/shop.service";
import {TabItem} from "../../../ui/tabs/item/item.component";
import {TabsService} from "../../../services/ui/tabs.service";
import {UiService} from "../../../ui.service";
import {OptionService} from "../../../services/option.service";
import {Option} from "../../../models/option";
import {ActivatedRoute} from "@angular/router";
import {TranslationService} from "../../../services/translation.service";
import {Error} from "tslint/lib/error";

@Component({
  selector: 'app-options-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class ShopOptionsIndexComponent implements OnInit, OnDestroy {

  public options: Option[] = [];

  public isLoading: boolean = false;

  public isLoadedAllRecords: boolean = false;

  private shopId: number = -1;

  constructor(
      private shopService: ShopService,
      private optionService: OptionService,
      private activatedRouter: ActivatedRoute,
      private translation: TranslationService,
      private ui: UiService
  ) { }

  ngOnInit() {

    this.optionService.onLoaded
        .do(response => this.isLoading = false)
        .do(response => this.isLoadedAllRecords = response.data.length < this.optionService.take)
        .subscribe();

    this.optionService.skip = 0;
    this.optionService.take = 15;
    this.activatedRouter.params.forEach(params => this.processParams(params));

    this.loadOptions();
    this.ui.setHaveContentAreaPaddings(true);
  }

  ngOnDestroy() {
    this.ui.setHaveContentAreaPaddings(false);
  }

  public loadOptions() {
    if (this.shopId < 0) {
      return;
    }

    this.isLoading = true;
    this.optionService.getShopOptions(this.shopId)
      .subscribe(option => this.options.push(option), error => this.processError(error));
  }

  public processParams(params) {
    this.shopId = Number(params.shop_id);
  }

  public processError(error: Error) {
    alert(this.translation.translate(error.message));
    this.isLoadedAllRecords = true;
    this.isLoading = true;
  }

}

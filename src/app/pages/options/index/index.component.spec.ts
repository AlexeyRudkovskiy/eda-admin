import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopOptionsIndexComponent } from './index.component';

describe('ShopOptionsIndexComponent', () => {
  let component: ShopOptionsIndexComponent;
  let fixture: ComponentFixture<ShopOptionsIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopOptionsIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopOptionsIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

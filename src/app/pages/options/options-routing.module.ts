import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ShopOptionsIndexComponent} from "./index/index.component";
import {EditOptionComponent} from "./edit/edit.component";
import {OptionsFormComponent} from "./form/form.component";

const routes: Routes = [
  {
    path: 'shops2/:shop_id/options',
    component: ShopOptionsIndexComponent
  },
  {
    path: 'shops/:shop_id/options/:option_id/edit',
    component: OptionsFormComponent,
    data: {
      type: 'edit'
    }
  },
  {
    path: 'shops/:shop_id/options/create',
    component: OptionsFormComponent,
    data: {
      type: 'create'
    }
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class OptionsRoutingModule { }

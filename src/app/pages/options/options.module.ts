import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShopOptionsIndexComponent } from './index/index.component';
import {UiModule} from "../../ui/ui.module";
import {MdButtonModule, MdInputModule, MdProgressSpinnerModule, MdSnackBarModule} from "@angular/material";
import { EditOptionComponent } from './edit/edit.component';
import {RouterModule} from "@angular/router";
import {OptionService} from "../../services/option.service";
import {FormsModule} from "@angular/forms";
import { OptionsFormComponent } from './form/form.component';

@NgModule({
  imports: [

    RouterModule,
    FormsModule,

    MdProgressSpinnerModule,
    MdButtonModule,
    MdInputModule,
    MdSnackBarModule,
    CommonModule,
    UiModule,
  ],
  declarations: [ShopOptionsIndexComponent, EditOptionComponent, OptionsFormComponent],
  providers: [ OptionService ]
})
export class OptionsModule { }

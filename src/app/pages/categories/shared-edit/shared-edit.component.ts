import { Component, OnInit } from '@angular/core';
import {Category} from "../../../models/category";
import {ActivatedRoute} from "@angular/router";
import {CategoryService} from "../../../services/category.service";
import {BaseComponent} from "../../../base-component";
import {MdSnackBar} from "@angular/material";

@Component({
  selector: 'app-shared-edit',
  templateUrl: './shared-edit.component.html',
  styleUrls: ['./shared-edit.component.scss'],
  providers: [ CategoryService ]
})
export class SharedEditComponent extends BaseComponent implements OnInit {

  public category: Category = new Category();

  public isEditing = false;

  public categorySaveSpinnerVisible: boolean = false;

  constructor(
    private activatedRouter: ActivatedRoute,
    private categoryService: CategoryService,
    private snackBar: MdSnackBar
  ) {
    super();
  }

  ngOnInit() {
    this.activatedRouter.params
      .pluck('id')
      .map(id => Number(id))
      .filter(id => id > 0)
      .do(id => this.isEditing = true)
      .switchMap(id => this.categoryService.getCategory(-1, id))
      .subscribe(category => this.category = category);
  }

  public updateCategory() {
    const endpoint = this.isEditing ?
      this.categoryService.updateShared(this.category) :
      this.categoryService.createShared(this.category);

    this.categorySaveSpinnerVisible = true;

    endpoint
      .finally(() => this.notifyCategoryWasUpdated())
      .subscribe(category => this.category = category);
  }

  private notifyCategoryWasUpdated() {
    const message = this.isEditing ? 'Category was successfully updated' : 'Category was successfully created';

    this.categorySaveSpinnerVisible = false;
    this.isEditing = true;
    this.snackBar.open(message, null, {
      duration: 1000
    });
  }

}

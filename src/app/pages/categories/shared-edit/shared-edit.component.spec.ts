import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedEditComponent } from './shared-edit.component';

describe('SharedEditComponent', () => {
  let component: SharedEditComponent;
  let fixture: ComponentFixture<SharedEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharedEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

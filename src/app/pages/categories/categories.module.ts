import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateCategoryComponent } from './create/create.component';
import {MdButtonModule, MdInputModule} from "@angular/material";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {UiModule} from "../../ui/ui.module";
import { EditCategoryComponent } from './edit/edit.component';
import { SharedListComponent } from './shared-list/shared-list.component';
import { SharedEditComponent } from './shared-edit/shared-edit.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,

    MdInputModule,
    MdButtonModule,

    UiModule,
  ],
  declarations: [CreateCategoryComponent, EditCategoryComponent, SharedListComponent, SharedEditComponent]
})
export class CategoriesModule { }

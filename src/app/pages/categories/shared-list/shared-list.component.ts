import {Component, OnDestroy, OnInit} from '@angular/core';
import {CategoryService} from "../../../services/category.service";
import {BaseComponent} from "../../../base-component";
import {Category} from "../../../models/category";

@Component({
  selector: 'app-shared-list',
  templateUrl: './shared-list.component.html',
  styleUrls: ['./shared-list.component.scss'],
  providers: [ CategoryService ]
})
export class SharedListComponent extends BaseComponent implements OnInit, OnDestroy {

  public categories: Category[] = [];

  constructor(private categoryService: CategoryService) {
    super();
  }

  ngOnInit() {
    this.categoryService.getSharedCategories(0)
      .subscribe(category => this.categories.push(category));
  }

  public deleteCategory(category: Category, index: number) {
    this.categoryService.deleteShared(category)
      .subscribe(data => {
        console.log(this.categories, index);
        this.categories.splice(index, 1)
      });
  }

}

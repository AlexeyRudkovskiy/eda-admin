import { Component, OnInit } from '@angular/core';
import {CategoryService} from "../../../services/category.service";
import {ShopService} from "../../../services/shop.service";
import {ActivatedRoute} from "@angular/router";
import {UiService} from "../../../ui.service";
import {BaseComponent} from "../../../base-component";
import {Location} from "@angular/common";
import {Shop} from "../../../models/shop";
import {Category} from "../../../models/category";
import {MdSnackBar} from "@angular/material";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  providers: [ CategoryService ]
})
export class EditCategoryComponent extends BaseComponent implements OnInit {

  public shop: Shop = null;

  public category: Category = new Category;

  public categorySaveSpinnerVisible: boolean = false;

  private shopId: number = 0;

  private categoryId: number = 0;

  constructor(
    private categoryService: CategoryService,
    private shopService: ShopService,
    private activatedRouter: ActivatedRoute,
    private uiService: UiService,
    private snackBar: MdSnackBar
  ) {
    super();
  }

  ngOnInit() {
    this.uiService.setHaveContentAreaPaddings(true);

    let paramsSubscription = this.activatedRouter.params
      .do(params => this.processParams(params))
      .switchMap(params => this.shopService.getShop(this.shopId))
      .do(shop => this.shop = shop)
      .switchMap(shop => this.categoryService.getCategory(this.shopId, this.categoryId))
      .subscribe(category => this.category = category.clone<Category>());

    this.registerSubscription(paramsSubscription);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();

    this.uiService.setHaveContentAreaPaddings(false);
  }

  public updateCategory() {
    this.categoryService.update(this.shopId, this.category)
      .subscribe(null, null, () => this.notifyCategoryUpdated());
  }

  private processParams(params: any) {
    const shopId = Number(params.shop_id);
    const categoryId = Number(params.id);

    this.shopId = shopId;
    this.categoryId = categoryId;

    return { shopId, categoryId };
  }

  private notifyCategoryUpdated() {
    this.snackBar.open('Категория успешно обновлена', null, {
      duration: 1000
    });
  }

}

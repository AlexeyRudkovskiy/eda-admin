import { Component, OnInit } from '@angular/core';
import {CategoryService} from "../../../services/category.service";
import {BaseComponent} from "../../../base-component";
import {ActivatedRoute} from "@angular/router";
import {ShopService} from "../../../services/shop.service";
import {Shop} from "../../../models/shop";
import {UiService} from "../../../ui.service";
import {Location} from "@angular/common";
import {Category} from "../../../models/category";
import {MdSnackBar} from "@angular/material";

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  providers: [ CategoryService ]
})
export class CreateCategoryComponent extends BaseComponent implements OnInit {

  public shopId: number = 0;

  public shop: Shop = null;

  public categorySaveSpinner: boolean = false;

  public name: string;

  public createdCategory: Category = null;

  constructor(
    private categoryService: CategoryService,
    private shopService: ShopService,
    private activatedRouter: ActivatedRoute,
    private uiService: UiService,
    private location: Location,
    private snackBar: MdSnackBar
  ) {
    super();
  }

  ngOnInit() {
    this.uiService.setHaveContentAreaPaddings(true);

    let paramsSubscription = this.activatedRouter.params
      .pluck('shop_id')
      .map(id => Number(id))
      .do(id => this.shopId = id)
      .switchMap(id => this.shopService.getShop(id))
      .subscribe(shop => this.shop = shop);

    this.registerSubscription(paramsSubscription);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();

    this.uiService.setHaveContentAreaPaddings(false);
  }

  public createCategory() {
    this.categorySaveSpinner = true;

    let createCategorySubscription = this.categoryService.createCategory(this.shopId, this.name)
      .do(category => this.categorySaveSpinner = false)
      .do(category => this.createdCategory = category)
      .finally(() => this.notifyCategoryCreated())
      .subscribe(category => this.location.back());

    this.registerSubscription(createCategorySubscription);
  }

  private notifyCategoryCreated() {
    this.snackBar.open(`Category '${this.createdCategory.name}' successfully created`, null, {
      duration: 1000
    });
  }

}

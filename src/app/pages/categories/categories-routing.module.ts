import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from "../../auth.guard";
import {CreateCategoryComponent} from "./create/create.component";
import {EditCategoryComponent} from "./edit/edit.component";
import {SharedListComponent} from "./shared-list/shared-list.component";
import {SharedEditComponent} from "./shared-edit/shared-edit.component";

const routes: Routes = [
  {
    path: 'shops/:shop_id/categories/create',
    component: CreateCategoryComponent
  },
  {
    path: 'shops/:shop_id/categories/:id',
    component: EditCategoryComponent
  },
  {
    path: 'categories/shared',
    component: SharedListComponent
  },
  {
    path: 'categories/shared/:id',
    component: SharedEditComponent
  },
  {
    path: 'categories/shared/create',
    component: SharedEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CategoriesRoutingModule { }

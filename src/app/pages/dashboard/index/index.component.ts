import { Component, OnInit } from '@angular/core';
import {UiService} from "../../../ui.service";
import {Router} from "@angular/router";
import {NotificationService} from "../../../services/notification.service";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class DashboardIndexComponent implements OnInit {

  constructor(
      private router:Router,
      private notifications: NotificationService
  ) { }

  ngOnInit() {
    this.notifications.eventsObservable
        .subscribe(event => console.log('event', event));
  }

}

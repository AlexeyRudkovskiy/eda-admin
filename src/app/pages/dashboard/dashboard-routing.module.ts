import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardIndexComponent} from "./index/index.component";

const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardIndexComponent,
    children: []
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }

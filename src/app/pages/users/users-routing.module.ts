import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from "../../auth.guard";
import {UsersIndexComponent} from "./index/index.component";
import {UsersEditComponent} from "./edit/edit.component";

const routes: Routes = [
  {
    path: 'users',
    component: UsersIndexComponent,
    children: [ ],
    canActivate: [ AuthGuard ],
    canActivateChild: [ AuthGuard ]
  },
  {
    path: 'users/create',
    component: UsersEditComponent
  },
  {
    path: 'users/:id/edit',
    component: UsersEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }


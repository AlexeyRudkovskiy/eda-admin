import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersIndexComponent } from './index/index.component';
import {MdButtonModule, MdInputModule, MdProgressSpinnerModule} from "@angular/material";
import {UiModule} from "../../ui/ui.module";
import { UsersEditComponent } from './edit/edit.component';
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,

    UiModule,

    MdInputModule,
    MdProgressSpinnerModule,
    MdButtonModule,
  ],
  declarations: [UsersIndexComponent, UsersEditComponent]
})
export class UsersModule { }

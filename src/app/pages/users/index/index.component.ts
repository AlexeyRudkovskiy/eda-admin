import { Component, OnInit } from '@angular/core';
import {UserService} from "../../../services/user.service";
import {User} from "../../../models/user";
import {BaseComponent} from "../../../base-component";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers: [ UserService ]
})
export class UsersIndexComponent extends BaseComponent implements OnInit {

  public isLoading: boolean = true;

  public hasMoreUser: boolean = true;

  public users: User[] = [];

  public take: number = 100;

  constructor(private userService: UserService, private router: Router, private activatedRoute: ActivatedRoute) { super(); }

  ngOnInit() {
    const administratorsSubscription = this.userService.administrators(this.take)
      .subscribe(user => this.users.push(user));
    const loadedEventSubscription = this.userService.loadedEvent
      .do(data => this.isLoading = false)
      .subscribe(data => this.hasMoreUser = data.pagination.size === data.pagination.take);
    this.registerSubscription(administratorsSubscription);
    this.registerSubscription(loadedEventSubscription);
  }

  public loadUsers() {
    this.isLoading = true;

    const administratorsSubscription = this.userService.administrators(this.take)
      .subscribe(user => this.users.push(user));
    this.registerSubscription(administratorsSubscription);
  }

  public editUser(user: User) {
    this.router.navigate([ 'users', user.id, 'edit' ]);
  }

}

import {Component, OnDestroy, OnInit} from '@angular/core';
import {UiService} from "../../../ui.service";
import {BaseComponent} from "../../../base-component";
import {UserService} from "../../../services/user.service";
import {ActivatedRoute} from "@angular/router";
import {User} from "../../../models/user";
import {MdSnackBar} from "@angular/material";
import {TranslationService} from "../../../services/translation.service";
import {Image} from "../../../models/image";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  providers: [ UserService ]
})
export class UsersEditComponent extends BaseComponent implements OnInit, OnDestroy {

  public user: User = new User;

  public spinnerVisible: boolean = false;

  public shops: any = { selected: [], removed: [] };

  constructor(
    public uiService: UiService,
    public userService: UserService,
    public activatedRouter: ActivatedRoute,
    public snackBar: MdSnackBar,
    public translation: TranslationService
  ) {
    super();
  }

  ngOnInit() {
    this.uiService.setHaveContentAreaPaddings(true);

    const getUserSubscription = this.activatedRouter.params
      .pluck('id')
      .map(id => Number(id))
      .map(id => isNaN(id) ? 0 : id)
      .flatMap(id => this.userService.getUser(id))
      .do(user => console.log(user))
      .subscribe(user => this.user = user);
    this.registerSubscription(getUserSubscription);
  }

  ngOnDestroy() {
    this.uiService.setHaveContentAreaPaddings(false);
  }

  public save() {

    this.spinnerVisible = true;
    let data = {
      first_name: this.user.firstName,
      last_name: this.user.lastName,
      phone_number: this.user.phone,
      password: this.user.password,
      shops: this.shops,
      type: 'Admin'
    };

    if (this.user.avatar instanceof File) {
      data['avatar'] = this.user.avatar;
    }

    const saveUserSubscription = this.userService.save(this.user.id, data)
      .do(user => this.spinnerVisible = false)
      .finally(() => this.notify())
      .subscribe(user => this.user = user);
    this.registerSubscription(saveUserSubscription);
  }

  public shopsSelected(ids: number[]) {
    this.shops = ids;
  }

  private notify() {
    this.snackBar.open(this.translation.translate('app.global.notification.updated'), null, {
      duration: 1000
    });
    this.user.password = null;
  }

  avatarDropped($event: Image) {
    this.user.avatar = $event;
  }
}

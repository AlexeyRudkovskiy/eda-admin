import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ShopsModule} from "./shops/shops.module";
import {ShopsRoutingModule} from "./shops/shops-routing.module";
import {ProductsModule} from "./products/products.module";
import {DashboardModule} from "./dashboard/dashboard.module";
import {DashboardRoutingModule} from "./dashboard/dashboard-routing.module";
import {ProductsRoutingModule} from "./products/products-routing.module";
import {ShopService} from "../services/shop.service";
import {ProductService} from "../services/product.service";
import { ProductShowSizesComponent } from './products/show/sizes/sizes.component';
import {OptionsModule} from "./options/options.module";
import {OptionsRoutingModule} from "./options/options-routing.module";
import {SizesModule} from "./sizes/sizes.module";
import {SizesRoutingModule} from "./sizes/sizes-routing.module";
import {OrdersModule} from "./orders/orders.module";
import {OrdersRoutingModule} from "./orders/orders-routing.module";
import {AddressesModule} from "./addresses/addresses.module";
import {AddressesRoutingModule} from "./addresses/addresses-routing.module";
import {AdvertisingModule} from "./advertising/advertising.module";
import {AdvertisingRoutingModule} from "./advertising/advertising-routing.module";
import {UsersModule} from "./users/users.module";
import {UsersRoutingModule} from "./users/users-routing.module";
import {PromoCodesModule} from "./promo-codes/promo-codes.module";
import {PromoCodesRoutingModule} from "./promo-codes/promo-codes-routing.module";

@NgModule({
  imports: [
    CommonModule,

    DashboardModule,
    DashboardRoutingModule,

    ShopsModule,
    ShopsRoutingModule,

    ProductsModule,
    ProductsRoutingModule,

    OptionsModule,
    OptionsRoutingModule,

    SizesModule,
    SizesRoutingModule,

    OrdersModule,
    OrdersRoutingModule,

    AddressesModule,
    AddressesRoutingModule,

    AdvertisingModule,
    AdvertisingRoutingModule,

    UsersModule,
    UsersRoutingModule,

    PromoCodesModule,
    PromoCodesRoutingModule,
  ],
  exports: [
    DashboardModule,
    DashboardRoutingModule,

    ShopsModule,
    ShopsRoutingModule,

    ProductsModule,
    ProductsRoutingModule,

    OptionsModule,
    OptionsRoutingModule,

    SizesModule,
    SizesRoutingModule,

    OrdersModule,
    OrdersRoutingModule,

    AddressesModule,
    AddressesRoutingModule,

    AdvertisingModule,
    AdvertisingRoutingModule,

    UsersModule,
    UsersRoutingModule,

    PromoCodesModule,
    PromoCodesRoutingModule,
  ],
  declarations: [  ],
  providers: [
    ShopService,
    ProductService
  ]
})
export class PagesModule { }

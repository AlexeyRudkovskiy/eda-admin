import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from "../../auth.guard";
import {ShowProductSizeComponent} from "./show/show.component";
import {CreateSizeComponent} from "./create/create.component";

const routes: Routes = [
  {
      path: 'shops/:shop_id/products/:product_id/sizes/create',
      component: CreateSizeComponent,
      children: [  ]
  },
  {
    path: 'shops/:shop_id/products/:product_id/sizes/:size_id',
    component: ShowProductSizeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class SizesRoutingModule { }


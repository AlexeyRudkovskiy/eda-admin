import { Component, OnInit, OnDestroy } from '@angular/core';
import {UiService} from "../../../ui.service";
import {ProductService} from "../../../services/product.service";
import {ActivatedRoute} from "@angular/router";

import 'rxjs/add/operator/switchMap'
import {Product} from "../../../models/product";
import {SizeService} from "../../../services/size.service";
import {Size} from "../../../models/size";
import {OptionService} from "../../../services/option.service";
import {Option} from "../../../models/option";
import {MdSnackBar} from "@angular/material";
import {TranslationService} from "../../../services/translation.service";

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.scss'],
  providers: [ SizeService ]
})
export class ShowProductSizeComponent implements OnInit, OnDestroy {

  public product: Product = null;

  public size: Size = new Size;

  private sizeId: number = -1;

  public optionsPrice: number = 0;

  public options: Option[] = [];

  public currentOptionsIds: number[] = [];

  constructor(
    private ui: UiService,
    private sizeService: SizeService,
    private productService: ProductService,
    private optionService: OptionService,
    private activatedRouter: ActivatedRoute,
    private translationService: TranslationService,
    private snackBar: MdSnackBar
  ) { }

  ngOnInit() {
    this.ui.setHaveContentAreaPaddings(true);

    this.activatedRouter.params
      .do(params => this.sizeId = params.size_id)
      .switchMap(params => this.productService.getProduct(params.shop_id, params.product_id))
      .do(product => this.product = product)
      .switchMap(product => this.sizeService.getSize(Number(this.sizeId), product))
      .do(size => this.size = size)
      .map(size => size.options)
      .concatMap(option => option)
      .do(option => option.selected = true)
      .do(option => this.options.push(option))
      .do(option => this.optionsPrice += option.price.amount - option.price.discount)
      .do(option => this.currentOptionsIds.push(option.id))
      .switchMap(() => this.optionService.getAllShopOptions(this.product.shop.id))
      .filter(option => !this.currentOptionsIds.includes(option.id))
      .subscribe(option => this.options.push(option));
  }

  ngOnDestroy() {
    this.ui.setHaveContentAreaPaddings(false);
  }

  public updateOptionsPrice(option) {
    this.optionsPrice = 0;
    this.options
      .filter(option => option.selected)
      .map(option => option.price)
      .forEach(price => this.optionsPrice += price.amount - price.discount);
  }

  public get totalPrice() {
    return this.size.price.amount - this.size.price.discount + this.optionsPrice;
  }

  public saveSize() {
    let optionsIds = this.options
      .filter(option => option.selected)
      .map(option => option.id);
    this.sizeService.update(this.product.shop.id, this.size, optionsIds)
      .finally(() => this.notifySizeUpdated())
      .subscribe(size => { /* empty */ });
  }

  private notifySizeUpdated() {
    this.snackBar.open(this.translationService.translate('app.size.updated'), null, {
      duration: 1000
    });
  }

}

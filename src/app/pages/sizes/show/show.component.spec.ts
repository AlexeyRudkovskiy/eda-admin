import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowProductSizeComponent } from './show.component';

describe('ShowProductSizeComponent', () => {
  let component: ShowProductSizeComponent;
  let fixture: ComponentFixture<ShowProductSizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowProductSizeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowProductSizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

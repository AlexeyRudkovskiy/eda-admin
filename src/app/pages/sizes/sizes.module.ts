import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowProductSizeComponent } from './show/show.component';
import { CreateSizeComponent } from './create/create.component';
import {MdButtonModule, MdCheckboxModule, MdInputModule} from "@angular/material";
import {FormsModule} from "@angular/forms";
import {UiModule} from "../../ui/ui.module";

@NgModule({
  imports: [
    CommonModule,
    MdCheckboxModule,
    MdButtonModule,
    MdInputModule,
    FormsModule,
    UiModule,
  ],
  exports: [
    MdCheckboxModule,
    MdButtonModule,
    MdInputModule,
  ],
  declarations: [ShowProductSizeComponent, CreateSizeComponent]
})
export class SizesModule { }

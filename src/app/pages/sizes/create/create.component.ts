import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {CreateSizeInterface, SizeService} from "../../../services/size.service";
import {Option} from "../../../models/option";
import {Price} from "../../../models/price";
import {OptionService} from "../../../services/option.service";
import {Location} from "@angular/common";
import {Size} from "../../../models/size";
import {TranslationService} from "../../../services/translation.service";
import {MdSnackBar} from "@angular/material";

@Component({
  selector: 'app-create-size',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  providers: [ SizeService ]
})
export class CreateSizeComponent implements OnInit {

  public options: Option[] = [];

  public title: string;

  public description: string;

  public price: Price = new Price;

  public optionsPrice: number = 0;

  private shopId: number = -1;

  private productId: number = -1;

  constructor(
      private activatedRoute: ActivatedRoute,
      private router: Router,
      private sizeService: SizeService,
      private optionService: OptionService,
      private location: Location,
      private translationService: TranslationService,
      private snackBar: MdSnackBar
  ) {
  }

  ngOnInit() {
    this.activatedRoute.params
      .map(params => this.processParams(params))
      .switchMap(params => this.optionService.getAllShopOptions(params.shopId))
      .subscribe(option => this.options.push(option));
  }

  public processParams(params) {
    let shopId = Number(params.shop_id);
    let productId = Number(params.product_id);

    this.shopId = shopId;
    this.productId = productId;

    return { shopId, productId };
  }

  public saveSize() {
    const redirectUrl = `/shops/${this.shopId}/products/${this.productId}/sizes/`;

    const options = this.options
      .filter(option => option.selected)
      .map(option => option.id);

    const createSize = {
      title: this.title,
      price: this.price,
      options: options
    } as CreateSizeInterface;

    this.sizeService.create(createSize, this.shopId, this.productId)
      .finally(() => this.notifySizeCreated())
      .subscribe(size => this.router.navigate([redirectUrl, size.id || 1]));
  }

  public updateOptionsPrice(option) {
    this.optionsPrice = 0;
    this.options
        .filter(option => option.selected)
        .map(option => option.price)
        .forEach(price => this.optionsPrice += price.amount - price.discount);
  }

  public get totalPrice() {
    return this.price.amount - this.price.discount + this.optionsPrice;
  }

  private notifySizeCreated() {
    this.snackBar.open(this.translationService.translate('app.size.created'), null, {
      duration: 1000
    });
  }

}

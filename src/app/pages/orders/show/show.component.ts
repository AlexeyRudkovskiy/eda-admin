import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {BaseComponent} from "../../../base-component";
import {ShopService} from "../../../services/shop.service";
import {Shop} from "../../../models/shop";
import {OrderService} from "../../../services/order.service";
import {Order} from "../../../models/order";
import {UiService} from "../../../ui.service";
import {MapsService} from "../../../services/maps.service";
import {Status} from "../../../models/status";
import {MdSnackBar} from "@angular/material";
import {APIService} from "../../../api.service";

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.scss'],
  providers: [ OrderService, MapsService ]
})
export class ShowOrderComponent extends BaseComponent implements OnInit {

  public shop: Shop = new Shop;

  public order: Order = new Order;

  public shopId: number = -1;

  public orderId: number = -1;

  public mapsPath: string;

  constructor(
    private activatedRouter: ActivatedRoute,
    private shopService: ShopService,
    private orderService: OrderService,
    private uiService: UiService,
    private mapsService: MapsService,
    private snackBar: MdSnackBar
  ) {
    super();
  }

  ngOnInit() {
    this.uiService.setHaveContentAreaPaddings(true);

    let paramsSubscription = this.activatedRouter.params
      .map(params => this.processParams(params))
      .switchMap(params => this.shopService.getShop(this.shopId))
      .do(shop => this.shop = shop)
      .switchMap(shop => this.orderService.getOrder(this.shopId, this.orderId))
      .do(order => this.processOrder(order))
      .subscribe(order => this.order = order);

    this.registerSubscription(paramsSubscription);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();

    this.uiService.setHaveContentAreaPaddings(false);
  }

  public statusExist(status: string): boolean {
    let statusObject = (this.order as any)[status];
    return typeof statusObject !== "undefined" && statusObject !== null && (typeof (statusObject as Status).done === 'string') && (statusObject as Status).completed;
  }

  public updateStatus(status: string) {
    this.orderService.updateStatus(this.shopId, this.order)
      .subscribe(null, null, () => this.processStatusUpdated());
  }

  private processOrder(order: Order) {
    this.mapsService.getStaticMap(300, 300, 17, order.address.toString())
      .subscribe(staticMap => this.mapsPath = staticMap);
  }

  private processParams(params) {
    const shopId: number = Number(params.shop_id);
    const orderId: number = Number(params.id);

    this.shopId = shopId;
    this.orderId = orderId;

    return { shopId, orderId };
  }

  private processStatusUpdated() {
    this.snackBar.open('Статус заказа успешно обновлён', null, { duration: 1000 });
  }

}

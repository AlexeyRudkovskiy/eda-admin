import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowOrderComponent } from './show/show.component';
import {UiModule} from "../../ui/ui.module";
import {TranslationDirective} from "../../ui/directives/translation.directive";

@NgModule({
  imports: [
    CommonModule,

    UiModule
  ],
  declarations: [ ShowOrderComponent ]
})
export class OrdersModule { }

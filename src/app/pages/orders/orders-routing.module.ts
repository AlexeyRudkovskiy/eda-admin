import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ShowOrderComponent} from "./show/show.component";

const routes: Routes = [
  {
    path: 'shops/:shop_id/orders/:id',
    component: ShowOrderComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }

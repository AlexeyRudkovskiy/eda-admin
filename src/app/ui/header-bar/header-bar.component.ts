import {Component, Input, OnInit} from '@angular/core';
import {Location} from "@angular/common";

@Component({
  selector: 'ui-header-bar',
  templateUrl: './header-bar.component.html',
  styleUrls: ['./header-bar.component.scss']
})
export class HeaderBarComponent implements OnInit {

  @Input() public text: string;

  constructor(
    private location: Location
  ) { }

  ngOnInit() {

  }

  public navigateBack() {
    this.location.back();
  }

}

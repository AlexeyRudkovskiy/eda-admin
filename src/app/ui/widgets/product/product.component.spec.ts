import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UIProductWidgetComponent } from './product.component';

describe('UIProductWidgetComponent', () => {
  let component: UIProductWidgetComponent;
  let fixture: ComponentFixture<UIProductWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UIProductWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UIProductWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

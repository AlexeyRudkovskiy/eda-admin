import {Component, Input, OnInit} from '@angular/core';
import {Product} from "../../../models/product";
import {Size} from "../../../models/size";

@Component({
  selector: 'ui-widget-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class UIProductWidgetComponent implements OnInit {

  @Input() public product: Product = new Product;

  constructor() { }

  ngOnInit() {
  }

  public getTotalSizePrice(size: Size) {
    let totalPrice: number = size.price.totalPrice;
    size.options
      .forEach(option => totalPrice += option.price.totalPrice);
    return totalPrice
  }

  public get totalPrice(): number {
    let totalPrice: number = 0;
    this.product.sizes
      .forEach(size => totalPrice += this.getTotalSizePrice(size));
    return totalPrice;
  }

}

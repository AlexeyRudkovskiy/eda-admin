import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UIUserWidgetComponent } from './user.component';

describe('UIUserWidgetComponent', () => {
  let component: UIUserWidgetComponent;
  let fixture: ComponentFixture<UIUserWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UIUserWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UIUserWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

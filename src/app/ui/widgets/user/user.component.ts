import {Component, Input, OnInit} from '@angular/core';
import {User} from "../../../models/user";
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'ui-widget-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UIUserWidgetComponent implements OnInit {

  @Input() public user: User;

  constructor() { }

  ngOnInit() {
  }

  public get basePath() {
    return environment.base_path.substr(0, environment.base_path.length - 1);
  }

  hasAvatar() {
    return typeof this.user.avatar !== "undefined" &&
      this.user.avatar !== null &&
      typeof this.user.avatar.small !== "undefined" &&
      this.user.avatar.small !== null;
  }
}

import {Component, EventEmitter, OnInit, OnDestroy, Output} from '@angular/core';
import {Location} from "../../models/location";

@Component({
  selector: 'ui-map-selection',
  templateUrl: './map-selection.component.html',
  styleUrls: ['./map-selection.component.scss']
})
export class MapSelectionComponent implements OnInit, OnDestroy {

  @Output() public markerLocationChanged: EventEmitter<Location> = new EventEmitter();

  public location: Location = new Location();

  constructor() {
    this.location.lat = 47.8388;
    this.location.lng = 35.139567;
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.markerLocationChanged.complete();
    this.markerLocationChanged.unsubscribe();
  }

  public onMarkedDragEnd(data: any) {
    this.markerLocationChanged.next(Location.fromObject<Location>(data.coords));
  }

}

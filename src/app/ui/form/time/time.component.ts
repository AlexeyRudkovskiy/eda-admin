import {Component, EventEmitter, Input, OnInit, OnDestroy, Output} from '@angular/core';

@Component({
  selector: 'app-time',
  templateUrl: './time.component.html',
  styleUrls: ['./time.component.scss']
})
export class TimeComponent implements OnInit, OnDestroy {

  @Input() public label: string;

  @Output() public timeChanged: EventEmitter<string> = new EventEmitter();

  public hours: number = 8;

  public minutes: number = 0;

  constructor() { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.timeChanged.complete();
    this.timeChanged.unsubscribe();
  }

  public timeWasChanged() {
    this.timeChanged.next(`${this.hours}:${this.minutes}`);
  }

}

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TranslationService} from "../../../services/translation.service";

@Component({
  selector: 'ui-form-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.scss']
})
export class DateComponent implements OnInit {

  public day: number = -1;

  public month: number = -1;

  public year: number = -1;

  @Output('date-changed') public dateChanged:EventEmitter<Date> = new EventEmitter<Date>();

  @Input() public label: string = '';

  @Input('label-hint') public hint: string = '';

  public months = [
      'app.global.month.january',
      'app.global.month.february',
      'app.global.month.march',
      'app.global.month.april',
      'app.global.month.may',
      'app.global.month.june',
      'app.global.month.july',
      'app.global.month.august',
      'app.global.month.september',
      'app.global.month.october',
      'app.global.month.november',
      'app.global.month.december'
  ];

  constructor(private translationService: TranslationService) { }

  ngOnInit() {
      this.months = this.months.map(item => this.translationService.translate(item));
  }

  public range(from: number, until: number) {
    if (from > 0 && until > from) {
      const items = [];
      for (let i = from; i <= until; i++) {
        items.push(i);
      }
      return items;
    }
    return [];
  }

  public get currentYear() {
    return new Date().getFullYear();
  }

  public get maxDayInMonth() {
    return new Date(this.year, this.month, 0).getDate();
  }

  public yearChanged(year: any) {
      this.year = year.value;
      this.emitDateChanged();
  }

  public monthChanged(month: any) {
      this.month = month.value;
      this.emitDateChanged();
  }

  public dayChanged(day: any) {
      this.day = day.value;
      this.emitDateChanged();
  }

  private emitDateChanged() {
      if (this.year < 0 || this.month < 0 || this.day < 0) {
          return;
      }

      this.dateChanged.emit(new Date(this.year, this.month - 1, this.day + 1, 0, 0, 0, 0));
  }

}

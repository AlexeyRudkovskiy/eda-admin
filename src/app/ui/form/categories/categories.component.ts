import {Component, Input, OnInit} from '@angular/core';
import {Category} from "../../../models/category";

@Component({
  selector: 'ui-form-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class FormCategoriesComponent implements OnInit {

  @Input() public categories: Category[] = [];

  constructor() { }

  ngOnInit() {
  }

  public toggleCategory(category) {
    category.selected = !category.selected;
  }

}

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ShopService} from "../../../services/shop.service";
import {Shop} from "../../../models/shop";

@Component({
  selector: 'ui-form-shops',
  templateUrl: './shops.component.html',
  styleUrls: ['./shops.component.scss']
})
export class ShopsComponent implements OnInit {

  @Input() set currentShops(shops: Shop[]) {
    if (typeof shops === "undefined" || shops === null) return;
    this.currentShopsIds = shops.map(shop => shop.id);
    console.log(this.currentShopsIds);
  };

  @Output() selected: EventEmitter<any> = new EventEmitter();

  public shops: Shop[] = [];

  public currentShopsIds: number[] = [];

  constructor(protected shopService: ShopService) { }

  ngOnInit() {
    this.shopService.getList(0, 0, [ 'name', 'id' ])
      .finally(() => this.markAsSelected())
      .subscribe(shop => this.shops.push(shop));
  }

  select(shop: Shop) {
    shop.selected = !shop.selected;
    let selectedShopsIds = this.shops.filter(shop => shop.selected).map(shop => shop.id);
    let removedShopsIds = this.currentShopsIds.filter(shop => !(selectedShopsIds.indexOf(shop) > -1));
    selectedShopsIds = selectedShopsIds.filter(id => this.currentShopsIds.indexOf(id) < 0);
    this.selected.emit({
      selected: selectedShopsIds,
      removed: removedShopsIds
    });
  }

  private markAsSelected() {
    this.shops.forEach(shop => shop.selected = this.currentShopsIds.indexOf(shop.id) > -1);
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormPriceComponent } from './price.component';

describe('FormPriceComponent', () => {
  let component: FormPriceComponent;
  let fixture: ComponentFixture<FormPriceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormPriceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

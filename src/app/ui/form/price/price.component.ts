import {Component, Input, OnInit} from '@angular/core';
import {Price} from "../../../models/price";

@Component({
  selector: 'ui-form-price',
  templateUrl: './price.component.html',
  styleUrls: ['./price.component.scss'],
})
export class FormPriceComponent implements OnInit {

  @Input() public price: Price = null;

  @Input() public disabled: boolean = false;

  @Input() public label: string = null;

  @Input() public hint: string = null;

  public currencies: any[] = [ {
    value: 'usd',
    label: 'Доллары'
  }, {
    value: 'uah',
    label: 'Гривны'
  }, {
    value: 'eur',
    label: 'Евро'
  } ];

  constructor() { }

  ngOnInit() {
  }

  public setPrice($event) {
    this.price.amount = parseFloat($event);
  }

  public setDiscount($event) {
    this.price.discount = parseFloat($event);
  }

  public setCurrency($event) {
    this.price.currency = $event;
  }

}

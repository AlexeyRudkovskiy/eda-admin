import {Component, EventEmitter, Input, OnInit, OnDestroy, Output} from '@angular/core';
import {Address, City, District} from "../../../models/address";
import {AddressService} from "../../../services/address.service";
import {MdSelectChange} from "@angular/material";
import {Location} from "../../../models/location";

@Component({
  selector: 'ui-form-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss'],
  providers: [ AddressService ]
})
export class AddressComponent implements OnInit, OnDestroy {

  @Input() public isHeaderVisible: boolean = true;

  @Input() public address: Address = new Address;

  @Output() public locationChanged: EventEmitter<Location> = new EventEmitter();

  @Output() public opensAtTimeChanged: EventEmitter<string> = new EventEmitter();

  @Output() public closesAtTimeChanged: EventEmitter<string> = new EventEmitter();

  public cities: City[] = [];

  public districts: District[] = [];

  public openedFrom: string;

  public openedUntil: string;

  constructor(
    private addressService: AddressService
  ) { }

  ngOnInit() {
    this.addressService.getCities()
      .subscribe(city => this.cities.push(city));
  }

  ngOnDestroy() {
    this.locationChanged.complete();
    this.locationChanged.unsubscribe();

    this.opensAtTimeChanged.complete();
    this.opensAtTimeChanged.unsubscribe();

    this.closesAtTimeChanged.complete();
    this.closesAtTimeChanged.unsubscribe();
  }

  public cityChanged(data: MdSelectChange) {
    this.districts.splice(0, this.districts.length);

    this.addressService.getDistricts(data.value)
      .subscribe(district => this.districts.push(district));
  }

  public markerLocationWasChanged(location: Location) {
    this.locationChanged.next(location);
  }

  public timeFromWasChanged(time) {
    time = this.processTime(time);
    this.openedFrom = time;
    this.opensAtTimeChanged.next(time);
  }

  public timeUntilWasChanged(time) {
    time = this.processTime(time);
    this.openedUntil = time;
    this.closesAtTimeChanged.next(time);
  }

  private processTime(time: string): string {
    let timeSplitted = time.split(':');
    timeSplitted[1] = '0' + timeSplitted[1];
    timeSplitted[1] = timeSplitted[1].substring(-2);
    return timeSplitted.join(':');
  }

}

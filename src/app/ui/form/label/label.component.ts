import {Component, Input, OnInit} from '@angular/core';
import {Label} from "../../../models/label";
import {LabelsService} from "../../../services/labels.service";

@Component({
  selector: 'ui-form-label',
  templateUrl: './label.component.html',
  styleUrls: ['./label.component.scss'],
  providers: [
    LabelsService
  ]
})
export class LabelComponent implements OnInit {

  @Input() public label: Label = new Label;

  @Input('shop-id') public shopId: number = -1;

  constructor(private labelsService: LabelsService) { }

  ngOnInit() {
  }

  public deleteLinkClicked() {
    this.labelsService.delete(this.shopId, this.label)
      .subscribe(() => this.label.clear());
  }

}

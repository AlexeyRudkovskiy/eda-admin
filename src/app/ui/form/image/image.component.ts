import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Image} from "../../../models/image";
import { environment } from "../../../../environments/environment";

@Component({
  selector: 'ui-form-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit {

  @Input() public showOnly: boolean = false;

  @Input() public photo: Image = null;

  @Input() public shopId: number = -1;

  @Input() public productId: number = -1;

  @Input() public field: string = null;

  @Output() public photoUploaded: EventEmitter<Image> = new EventEmitter();

  public rawPhotoPreview: any = null;

  private fileReader = new FileReader();

  public get hasPhoto() {
    return (
        (
            this.photo !== null &&
            typeof this.photo !== "undefined" &&
            typeof this.photo.small !== "undefined"
        ) && this.photo.small.length > 0
      ) || this.rawPhotoPreview !== null;
  }

  constructor() {
    this.fileReader = new FileReader();
    this.fileReader.addEventListener('loadend', () => this.fileReaderWasLoaded());
  }

  ngOnInit() {
  }

  public photoUploadedEvent(event) {
    this.photoUploaded.emit(event);
    this.photo = event;
  }

  public photoWasDropped(photo: File) {
    this.fileReader.readAsDataURL(photo);
    this.photoUploaded.emit(photo as any);
  }

  private fileReaderWasLoaded() {
    this.rawPhotoPreview = this.fileReader.result;
  }

  public getPhotoUrl(path: string|any = null) {
    
    if (this.rawPhotoPreview !== null) {
      return this.rawPhotoPreview;
    }
    
    if (typeof this.photo.rectangle !== "undefined" && this.photo.rectangle.length > 0) {
      path = this.photo.rectangle;
    } else {
      path = this.photo.small;
    }
    
    if (path !== null) {
      path = path.substring(1, path.length);
      return environment.base_path + path;
    }
  }

}

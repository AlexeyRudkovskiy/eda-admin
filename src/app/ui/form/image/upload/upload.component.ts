import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Image} from "../../../../models/image";

@Component({
  selector: 'ui-form-image-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadImageComponent implements OnInit {

  @Input() public showOnly: boolean = false;

  @Input() public shopId: number = -1;

  @Input() public productId: number = -1;

  @Input() public field: string = null;

  @Output() public photoUploaded: EventEmitter<Image> = new EventEmitter();

  @Output() public photoDropped: EventEmitter<File> = new EventEmitter();

  public indeterminate: boolean = false;

  protected photoWasUploaded: boolean = false;

  public progress: number = 0;

  public get uploadingProgressBarLabel() {
    if (this.photoWasUploaded == true) {
      this.indeterminate = false;
      return 'app.image.upload.status.uploaded';
    }
    if (this.progress === 100) {
      this.indeterminate = true;
    }
    return this.progress < 100 ? 'app.image.upload.status.uploading' : 'app.image.upload.status.processing';
  }

  constructor() { }

  ngOnInit() {
  }

  public photoWasDropped(photo) {
    this.photoDropped.emit(photo);
  }

  public photoWasUpdated(image: any) {
    this.photoWasUploaded = true;
    this.photoUploaded.emit(image);
  }

}

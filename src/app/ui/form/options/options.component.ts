import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Option} from "../../../models/option";

@Component({
  selector: 'ui-form-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.scss']
})
export class OptionsComponent implements OnInit {

  @Input() public options: Option[] = [];

  @Output() public selectionChanged: EventEmitter<Option> = new EventEmitter();

  public filter: string = null;

  constructor() { }

  ngOnInit() {
  }

  public select(option: Option) {
    option.selected = !option.selected;
    this.selectionChanged.next(option);
  }

}

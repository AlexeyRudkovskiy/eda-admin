import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TabsService} from "../../../services/ui/tabs.service";

export interface TabItem {
  text: String;
  link: String|String[];
  active: boolean;
  badge?: number;
  relativeUrl?: string;
}

@Component({
  selector: 'app-ui-tabs-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
  outputs: [ 'click' ]
})
export class TabItemComponent implements OnInit {

  @Input() public index: number;

  @Input() public schema: TabItem = null;

  constructor(private tabsService: TabsService) { }

  ngOnInit() {

  }

  public setActiveTab(schema: any) {
    this.tabsService.changeTab(this.schema);
  }

}

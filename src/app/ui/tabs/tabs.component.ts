import {Component, Input, OnInit, OnDestroy, Output, EventEmitter} from '@angular/core';
import {TabItem} from "./item/item.component";
import {TabsService} from "../../services/ui/tabs.service";
import {Observable} from "rxjs/Observable";
import {Subscription} from "rxjs/Subscription";
import {getMdTooltipInvalidPositionError} from "@angular/material";
import {Subject} from "rxjs/Subject";

import 'rxjs/add/operator/takeUntil'

@Component({
  selector: 'app-ui-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit, OnDestroy {

  @Input() public tabs: TabItem[] = [];

  @Input() public header: string = null;

  @Input() public subHeader: string = null;

  @Input() public subHeaderLink: string = null;

  @Output() public tabItemClicked: EventEmitter<TabItem> = new EventEmitter();

  private disposables: Subscription[] = [];

  private subscriptions: Subscription = new Subscription();

  constructor(private tabsService: TabsService) { }

  ngOnInit() {
    this.subscriptions.add(this.tabsService.tabsItems.subscribe(items => Array.prototype.push.apply(this.tabs, items)));
    this.subscriptions.add(this.tabsService.shouldClear.subscribe(items => this.tabs.splice(0, this.tabs.length)));

    this.tabsService.getTabChangedObservable().subscribe(clicked => this.tabs.forEach(tab => tab.active = tab === clicked));
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
    this.tabsService.resetTabChangedReplaySubject();
  }

}

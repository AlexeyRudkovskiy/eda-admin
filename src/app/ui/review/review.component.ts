import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'ui-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {

  @Input() public review: any;

  constructor() { }

  ngOnInit() {
  }

}

import { NgModule } from '@angular/core';
import {CommonModule, NgStyle} from '@angular/common';
import {TabsComponent} from "./tabs/tabs.component";
import {TabItemComponent} from "./tabs/item/item.component";
import {RouterModule} from "@angular/router";
import {TabsService} from "../services/ui/tabs.service";
import { ReviewComponent } from './review/review.component';
import { FormPriceComponent } from './form/price/price.component';
import {
  MdButtonModule, MdCheckboxModule, MdInputModule, MdProgressSpinnerModule,
  MdSelectModule
} from "@angular/material";
import {FormsModule} from "@angular/forms";
import { ImageComponent } from './form/image/image.component';
import { UploadImageComponent } from './form/image/upload/upload.component';
import {ImageUploadableDirective} from "./directives/image-uploadable.directive";
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { LabelComponent } from './form/label/label.component';
import { OptionsComponent } from './form/options/options.component';
import { HeaderBarComponent } from './header-bar/header-bar.component';
import {OptionsPipe} from "../pipes/options.pipe";
import { SaveComponent } from './form/save/save.component';
import { FormCategoriesComponent } from './form/categories/categories.component';
import { UIUserWidgetComponent } from './widgets/user/user.component';
import { UIProductWidgetComponent } from './widgets/product/product.component';
import { AddressComponent } from './form/address/address.component';
import { MapSelectionComponent } from './map-selection/map-selection.component';
import {AgmCoreModule} from "@agm/core";
import {environment} from "../../environments/environment";
import { TimeComponent } from './form/time/time.component';
import {TranslationDirective} from "./directives/translation.directive";
import { TranslationPipe } from './pipes/translation.pipe';
import { ShopsComponent } from './form/shops/shops.component';
import { DateComponent } from './form/date/date.component';

@NgModule({
  imports: [
    RouterModule,
    CommonModule,

    FormsModule,
    MdButtonModule,
    MdInputModule,
    MdCheckboxModule,
    MdSelectModule,
    MdProgressSpinnerModule,

    AgmCoreModule.forRoot({
      apiKey: environment.google_maps.api_key
    }),
  ],
  declarations: [
    TabsComponent,
    TabItemComponent,
    ReviewComponent,
    FormPriceComponent,
    ImageComponent,
    UploadImageComponent,

    ImageUploadableDirective,
    TranslationDirective,
    ProgressBarComponent,
    LabelComponent,
    OptionsComponent,
    HeaderBarComponent,
    OptionsPipe,
    SaveComponent,
    FormCategoriesComponent,
    UIUserWidgetComponent,
    UIProductWidgetComponent,
    AddressComponent,
    MapSelectionComponent,
    TimeComponent,

    TranslationPipe,

    ShopsComponent,

    DateComponent,
  ],
  exports: [
    TabsComponent,
    TabItemComponent,
    ReviewComponent,
    FormPriceComponent,
    ImageComponent,
    UploadImageComponent,
    ProgressBarComponent,
    LabelComponent,
    OptionsComponent,
    HeaderBarComponent,
    SaveComponent,
    FormCategoriesComponent,
    ShopsComponent,

    AddressComponent,

    UIUserWidgetComponent,
    UIProductWidgetComponent,

    TranslationDirective,
    TranslationPipe,

    DateComponent
  ],
  providers: [
    TabsService
  ]
})
export class UiModule { }

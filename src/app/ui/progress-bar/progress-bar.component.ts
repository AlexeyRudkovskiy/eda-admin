import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'ui-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss']
})
export class ProgressBarComponent implements OnInit {

  @Input() public percents: number = 0;

  @Input() public label: number = 0;

  @Input() public indeterminate: boolean = false;

  constructor() { }

  ngOnInit() {
  }

}

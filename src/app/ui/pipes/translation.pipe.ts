import { Pipe, PipeTransform } from '@angular/core';
import {TranslationService} from "../../services/translation.service";

@Pipe({
  name: 'translation'
})
export class TranslationPipe implements PipeTransform {

  public constructor(private translation: TranslationService) {

  }

  transform(value: any, args?: any): any {
    return this.translation.translate(value);
  }

}

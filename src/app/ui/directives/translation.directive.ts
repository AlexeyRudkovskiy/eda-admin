import {TranslationService} from "../../services/translation.service";
import {Directive, ElementRef, Input} from "@angular/core";

@Directive({
  selector: '[translation]'
})
export class TranslationDirective {

  private value: string;

  @Input() set translation(value: string) {
    this.element.nativeElement.innerHTML = this.translationService.translate(value);
  }

  constructor(private element:ElementRef, private translationService: TranslationService) { }

}

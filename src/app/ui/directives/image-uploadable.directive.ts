import {Directive, ElementRef, EventEmitter, HostListener, Input, Output, Renderer} from '@angular/core';
import {FileUploadingService} from "../../services/file-uploading.service";
import {Image} from "../../models/image";

@Directive({
  selector: '[appImageUploadable]',
  providers: [
      FileUploadingService
  ]
})
export class ImageUploadableDirective {

  @Input() public showOnly: boolean = false;

  @Input() public shopId: number = -1;

  @Input() public productId: number = -1;

  @Input() public field: string = null;

  @Output() public photoUploaded: EventEmitter<Image> = new EventEmitter();

  @Output() public uploadProgress: EventEmitter<number> = new EventEmitter();

  @Output() public photoDropped: EventEmitter<File> = new EventEmitter<File>();

  constructor(
      private uploadingService: FileUploadingService,
      private el: ElementRef
  ) {
    //
  }

  @HostListener('dragover', ['$event'])
  public onDragOver(event) {
    event.preventDefault();
    event.stopPropagation();
    this.el.nativeElement.classList.add('active');
  }

  @HostListener('dragleave', ['$event'])
  private onDragLeave(event) {
    event.preventDefault();
    event.stopPropagation();
    this.el.nativeElement.classList.remove('active');
  }

  @HostListener('drop', ['$event'])
  private onDrop(event) {
    this.onDragLeave(event);

    const dataTransfer = event.dataTransfer;
    const files = dataTransfer.files;
    if (files.length !== 1) {
      /// todo: Notify that more than one file or empty
      return;
    }

    const file = files[0];

    if (this.showOnly) {
      this.photoDropped.emit(file);
      return;
    }

    this.uploadingService.progressObservable
        .subscribe(progress => this.uploadProgress.emit(progress));

    this.uploadingService
        .uploadFile(this.shopId, this.productId, file, this.field, (response) => this.processResponse(response))
        .subscribe(photo => this.photoUploaded.emit(photo));
  }

  private processResponse (response: any) {
    if (this.field == 'poster') {
      return response.shopPoster;
    }
    if (this.shopId > 0 && this.productId == -1 && this.field != 'poster') {
      return response.shopImage;
    }
    return response.photo;
  }

}

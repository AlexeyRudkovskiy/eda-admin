import {Model, Parsable, ParseFromObject, ParseInnerObject} from "./model";
import { Image } from "./image";

export class ReviewAuthor extends Model {

    @Parsable
    public firstName: string;

    @Parsable
    public lastName: string;

    @Parsable
    @ParseInnerObject(Image, {
        field: 'avatar'
    })
    public avatar: Image;

}

export class Review extends Model {

    @Parsable
    public id: number;

    @Parsable
    @ParseInnerObject(ReviewAuthor, {
        field: 'author'
    })
    public author: ReviewAuthor;

    @Parsable
    public createdAt: string;

    @Parsable
    public comment: string;

    @Parsable
    public likes: number;

    @Parsable
    @ParseFromObject('commentsTotal')
    public commentsCount: number;

    private parseAuthor(author) {
        return ReviewAuthor.fromObject(author);
    }

}
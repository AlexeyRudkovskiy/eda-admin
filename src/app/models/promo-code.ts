import {Model, Parsable, ParseFromObject} from "./model";

export class PromoCode extends Model {

    @Parsable
    public id: number;

    @Parsable
    public name: string;

    @Parsable
    @ParseFromObject('expire_at', 'expireAtParser', 'expireAtSerializer')
    public expire_at: string = null;

    @Parsable
    public expire_after: number = null;

    @Parsable
    public discount: number = 0;

    @Parsable
    @ParseFromObject('shop_id')
    public shopId: number = 0;

    @Parsable
    public is_percents: boolean = false;

    public expireAtParser(expire_at: string) {
        return expire_at;
    }

    public expireAtSerializer(expire_at: string) {
        return expire_at;
    }

    public get discountSuffix() {
        return this.is_percents ? 'app.promo_code.discount.suffix.percents' : 'app.promo_code.discount.suffix.uah'
    }

}
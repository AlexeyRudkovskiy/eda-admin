import {Model, Parsable, ParseFromObject, ParseInnerObject} from "./model";
import {Image} from "./image";
import {Shop} from "./shop";

export class User extends Model {

  @Parsable
  public id: number;

  @Parsable
  @ParseInnerObject(Image)
  public avatar: Image = new Image;

  @Parsable
  public firstName: string;

  @Parsable
  public lastName: string;

  @Parsable
  @ParseFromObject('phoneNumber')
  public phone: string;

  @Parsable
  public password: string = null;

  public shops: Shop[] = [];

}

import {Model, Parsable, ParseFromObject, ParseInnerObject} from "./model";
import {Price} from "./price";
import {Option} from "./option"
export class Size extends Model {

    @Parsable
    id: number;

    @Parsable
    @ParseFromObject('options', 'parseOptions')
    options: Option[] = [];

    @Parsable
    @ParseInnerObject(Price)
    price: Price;

    @Parsable
    title: string;

    public constructor() {
        super();
        this.price = new Price;
    }

    public parseOptions(options): Option[] {
        return options.map(option => Option.fromObject<Option>(option));
    }

    public get optionsList(): string {
        return this.options.map(option => option.name).join(', ');
    }

}
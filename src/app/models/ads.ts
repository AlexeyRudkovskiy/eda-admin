import {Model, Parsable, ParseFromObject, ParseInnerObject} from "./model";
import {Image} from "./image";
import {environment} from "../../environments/environment";

export class Advertising extends Model {

  @Parsable
  public id: number = -1;

  @Parsable
  public name: string = null;

  @Parsable
  public title: string = null;

  @Parsable
  public content: string = null;

  @Parsable
  @ParseInnerObject(Image)
  public image: Image = new Image;

  @Parsable
  @ParseFromObject('is_active')
  public isActive: boolean = false;

  @Parsable
  public type: string;

  @Parsable
  @ParseFromObject('shopId')
  public shop_id: number = -1;

  @Parsable
  @ParseFromObject('productId')
  public product_id: number = -1;

  @Parsable
  @ParseFromObject('categoryId')
  public category_id: number = -1;

  public getImageUrl() {
    return `${environment.base_path}${this.image.large || ''}`;
  }

}
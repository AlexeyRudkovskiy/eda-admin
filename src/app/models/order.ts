import {Model, Parsable, ParseFromObject, ParseInnerObject} from "./model";
import {Address} from "./address";
import {Price} from "./price";
import {User} from "./user";
import {Product} from "./product";
import {Status} from "./status";

export class Order extends Model {

  @Parsable
  public id: number;

  @Parsable
  public deliveredTime: number;

  @Parsable
  public status: string;

  @Parsable
  @ParseInnerObject(Address)
  public address: Address = new Address();

  @Parsable
  @ParseInnerObject(Price)
  public price: Price = new Price;

  @Parsable
  @ParseInnerObject(User)
  public user: User = new User;

  @Parsable
  @ParseFromObject('products', 'parseProducts')
  public products: Product[] = [];

  @Parsable
  @ParseFromObject('productsQuantity')
  public productsQuantity: number = 0;

  @Parsable
  public paymentType: string = "online";

  @Parsable
  @ParseInnerObject(Status)
  public registered: Status = null;

  @Parsable
  @ParseInnerObject(Status)
  public processing: Status = null;

  @Parsable
  @ParseInnerObject(Status)
  public sent: Status = null;

  @Parsable
  @ParseInnerObject(Status)
  public received: Status = null;

  public parseProducts(products) {
    return products.map(product => Product.fromObject<Product>(product));
  }

}
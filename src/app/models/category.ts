import {Model, Parsable} from "./model";

export class Category extends Model {

    @Parsable
    public id: number;

    @Parsable
    public name: string;

    public selected: boolean = false;

    public shopId: number = null;

    @Parsable
    public using: boolean = false;

}
import {Model, Parsable} from "./model";

export class Location extends Model {

  @Parsable
  public lat: number;

  @Parsable
  public lng: number;

}
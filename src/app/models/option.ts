import {Model, Parsable, ParseInnerObject} from "./model";
import {Price} from "./price";

export class Option extends Model {

    @Parsable
    public id: number;

    @Parsable
    public description: string;

    @Parsable
    public name: string;

    @Parsable
    @ParseInnerObject(Price, 'price')
    public price: Price = new Price;

    public selected: boolean = false;

}
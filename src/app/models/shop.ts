import { Price } from "./price";
import { Image } from "./image";
import {Model, ParseFromObject, Parsable, ParseInnerObject} from "./model";
import {Label} from "./label";

export class Shop extends Model {

    @Parsable
    id: number;

    @Parsable
    deliveryPrice: Price;

    @Parsable
    deliveryTime: number;

    @Parsable
    minimalOrderPrice: Price;

    @Parsable
    @ParseInnerObject(Price, "minimal_free_delivery_price")
    minimalFreeDeliveryPrice: Price;

    @Parsable
    @ParseFromObject("name")
    shopName: string;

    @Parsable
    rating: number;

    @Parsable
    @ParseInnerObject(Image, 'shopImage')
    shopImage: Image;

    @Parsable
    @ParseInnerObject(Image, 'shopPoster')
    shopPoster: Image;

    @Parsable
    isFavorite: boolean;

    @Parsable
    @ParseInnerObject(Label)
    label: Label = new Label;

    selected: boolean = false;

    public parseShopImage(value: any): any {
        const image = new Image();
        image.small = value.web['1x'];
        image.medium = value.web['2x'];
        image.large = value.web['3x'];

        return image;
    }

}

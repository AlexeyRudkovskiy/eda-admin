import {Model, Parsable, ParseFromObject} from "./model";
export class Image extends Model {

    @Parsable
    @ParseFromObject('1x')
    small: string;

    @Parsable
    @ParseFromObject('2x')
    medium: string;

    @Parsable
    @ParseFromObject('3x')
    large: string;
    
    @Parsable
    @ParseFromObject('rectangle')
    rectangle: string = '';

    public filterObject(object: any): any {
        if (object === null) {
            object = { web: { /* empty */ } };
        }
      let imageData = typeof object['web'] !== "undefined" ? object.web : object;
      if (typeof object['rectangle'] !== "undefined") {
        imageData.rectangle = object['rectangle'];
      }
      return imageData;
    }

}

import {Model, Parsable, ParseFromObject} from "./model";
export class Price extends Model {

    @Parsable
    amount: number = 0;

    @Parsable
    currency: string = 'uah';

    @Parsable
    discount: number = 0;

    @Parsable
    @ParseFromObject('text')
    priceType: string;

    public get totalPrice(): number {
        return this.amount - this.discount;
    }

    public toString() {
        return this.totalPrice + ' ' + this.currency;
    }

}

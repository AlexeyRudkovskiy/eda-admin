import {Clonnable, Model, Parsable, ParseFromObject, ParseInnerObject} from "./model";
import {Label} from "./label";
import {Price} from "./price";
import {Image} from "./image";
import {Shop} from "./shop";
import {Size} from "./size";
import {Option} from "./option";
import {Category} from "./category";
export class Product extends Model {

    @Parsable
    public id: number;

    @Parsable
    @ParseInnerObject(Label)
    public label: Label = new Label();

    @Parsable
    @ParseFromObject('name')
    public productName: string;

    @Parsable
    public rating: number;

    @Parsable
    public measure: string;

    @Parsable
    @ParseInnerObject(Price)
    public price: Price;

    @Parsable
    @ParseInnerObject(Image, {
        field: 'photo'
    })
    public photo: Image;

    @Parsable
    @ParseFromObject('sizes', 'parseSizes')
    public sizes: Size[] = [];

    @Parsable
    @ParseFromObject('commentsTotal')
    public commentsCount: number;

    @Parsable
    public options: number;

    @Clonnable(Shop)
    public shop: Shop;

    @Parsable
    @ParseFromObject('categories')
    public categories: Category[] = [];

    @Parsable
    @ParseFromObject('consist_of')
    public consistOf: string = null;

    public constructor() {
        super();
        this.price = new Price();
        this.shop = new Shop();
    }

    public parseSizes(sizes) {
        // this.sizes = sizes.map(size => );
        return sizes.map(size => Size.fromObject<Size>(size));
    }

}
import {forEach} from "@angular/router/src/utils/collection";
import {ReviewAuthor} from "./review";
import {Image} from "./image"

export interface IParseInnerObjectParams {
    field?: string;
    mapToCallback?: string;
    serializeToCallback?: string
}

export interface IRelationParams {
    relatedTo: any;
}

interface FieldDescription {
    name: string;
    mapToCallback: string;
    serializeToCallback: string;
    relationParams: IRelationParams
}

export function Parsable(target: any, name: string) {
    if (typeof target.constructor.parsable === "undefined") {
        target.constructor.parsable = [];
    }
    target.constructor.parsable.push(name);
}

export function ParseFromObject(field: string, mapToCallback: string = null, serializeToCallback: string = null) {
    return function (target: any, name: string) {
        if (typeof target.constructor.fields === "undefined") {
            target.constructor.fields = {};
        }

        target.constructor.fields[field] = {
            name, mapToCallback, serializeToCallback, relationParams: null
        } as FieldDescription;
    }
}

export function ParseInnerObject<T extends Model>(related: typeof Model, params?: IParseInnerObjectParams) {
    const fieldDescription: FieldDescription = {
        name: params != null ? params.field || null : null,
        serializeToCallback: params != null ? params.serializeToCallback || null : null,
        mapToCallback: params != null ? params.mapToCallback || null : null,
        relationParams: {
            relatedTo: related
        } as IRelationParams
    } as FieldDescription;

    return function (target: any, name: string) {
        if (typeof target.constructor.fields === "undefined") {
            target.constructor.fields = {};
        }

        if (fieldDescription.name === null) {
            fieldDescription.name = name;
        }

        target.constructor.fields[name] = fieldDescription;
    }
}

export function Clonnable<T extends Model>(related: typeof Model) {
    return function (target: any, field: string) {
        if (typeof target.constructor.clonnable === "undefined") {
            target.constructor.clonnable = {};
        }

        target.constructor.clonnable[field] = related;
    }
}

export class Model {

    public static clonnable: any;

    public static fields: any;

    public static parsable: string[];

    public static fromObject<T extends Model>(object: any): T {
        const fields = typeof this.fields !== "undefined" ? this.fields : {};
        const clonnable = typeof this.clonnable !== "undefined" ? this.clonnable : {};

        if (object instanceof Model) {
            return object as T;
        }

        let instance: T = ((new this) as any) as T;
        object = instance.filterObject(object);

        for (let key in object) {
            let value: any = null;
            let name: string = null;
            if (typeof fields[key] !== "undefined") {
                const fieldDescription: FieldDescription = fields[key];
                name = fieldDescription.name;
                if (fieldDescription.mapToCallback !== null) {
                    value = instance[fieldDescription.mapToCallback].call(this, object[key]);
                } else if (fieldDescription.relationParams !== null) {
                    const relatedTo = fieldDescription.relationParams.relatedTo;
                    value = relatedTo.fromObject.call(relatedTo, object[key]);
                } else {
                    value = object[key];
                }
            } else {
                name = key;
                value = object[key];
            }

            instance[name] = value;
        }

        for (let key in clonnable) {
            if (typeof object[key] !== "undefined") {
                instance[key] = clonnable[key].fromObject(object[key]);
            }
        }

        return instance;
    }

    public filterObject (object: any): any {
        return object;
    }

    public toObject(): any {
        const constructor = (this.constructor as any);

        const fields = constructor.fields;
        const clonnable = typeof constructor.clonnable !== "undefined" ? constructor.clonnable : {};
        const parsable = constructor.parsable;
        const fieldsReversed = {};
        const object: any = {};

        for (let key in fields) {
            let value = fields[key];
            let reversedName = value.name;
            let reversedValue = key;
            let currentValue = this[reversedName];
            if (currentValue instanceof Model) {
                currentValue = currentValue.toObject();
            }
            fieldsReversed[reversedName] = reversedValue;
        }

        for (let key of parsable) {
            let name: string = typeof fieldsReversed[key] !== "undefined" ? fieldsReversed[key] : key;
            let value = null;
            if (this[key] instanceof Model) {
                value = this[key].toObject();
            } else {
                value = this[key];
            }

            object[name] = value;
        }

        for (let key in clonnable) {
            object[key] = (this[key] instanceof Model) ? this[key].toObject() : this[key];
        }

        return object;
    }

    public clone<T extends Model>(): T {
        const serialized = JSON.stringify(this.toObject());
        const deserialized = JSON.parse(serialized);
        return (this.constructor as any).fromObject(deserialized);
    }

}
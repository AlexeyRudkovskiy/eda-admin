import {Model, Parsable} from "./model";
export class Label extends Model {

  @Parsable
  public id: number;

  @Parsable
  public name: string = null;

  @Parsable
  public text: string = null;

  clear() {
    this.name = null;
    this.text = null;
    this.id = -1;
  }
}
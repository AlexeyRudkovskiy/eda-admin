import {Model, Parsable, ParseInnerObject} from "./model";

export class City extends Model {

  @Parsable
  public id: number;

  @Parsable
  public name: string;

}

export class District extends Model {

  @Parsable
  public id: number;

  @Parsable
  public name: string;

}

export class Address extends Model {

  @Parsable
  public id: number = -1;

  @Parsable
  public name: string = null;

  @Parsable
  @ParseInnerObject(City)
  public city: City = new City();

  @Parsable
  @ParseInnerObject(District)
  public district: District = new District();

  @Parsable
  public address: string = null;

  @Parsable
  public apartmentNumber: string = null;

  @Parsable
  public entrance: number = null;

  @Parsable
  public mailIndex: number = null;

  @Parsable
  public houseNumber: string = null;

  public toString(): string {
    let addressString = this.city.name + ', ' + this.district.name + ', ' + this.address + ' ';
    if (this.apartmentNumber !== null) {
      addressString += this.apartmentNumber + ', ';
    }

    if (this.houseNumber !== null) {
      addressString += this.houseNumber;
    }

    return addressString;
  }

}

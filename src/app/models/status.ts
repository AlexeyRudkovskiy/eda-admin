import {Model, Parsable} from "./model";

export class Status extends Model {

  @Parsable
  public completed: boolean;

  @Parsable
  public done: string|boolean = false;

}
import { Injectable } from '@angular/core';
import {ReplaySubject} from "rxjs/ReplaySubject";

@Injectable()
export class UiService {

  constructor() { }

  private _isHaveContentAreaPaddings: ReplaySubject<boolean> = new ReplaySubject();

  public setHaveContentAreaPaddings(value: boolean) {
    this._isHaveContentAreaPaddings.next(value);
  }

  get isHaveContentAreaPaddings(): ReplaySubject<boolean> {
    return this._isHaveContentAreaPaddings;
  }

}

import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {APIService} from "./api.service";

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

  constructor(
      private api: APIService,
      private router: Router
  ) {  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    this.api.getTokenChangedObservable().subscribe();

    const token = this.api.authToken || this.api.tempAuthToken;

    if (token !== null) {
      return true;
    } else {
      this.router.navigate([ '/auth/login' ]);
    }

    return true;
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.canActivate(childRoute, state);
  }

}

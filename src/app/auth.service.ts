import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import {APIService} from "./api.service";
import {Observable} from "rxjs/Observable";

import 'rxjs/add/operator/map'
import {APIResponse} from "./apiresponse";
import {SidebarService} from "./sidebar/sidebar.service";

@Injectable()
export class AuthService {

  constructor(
    private api: APIService,
    private sidebarService: SidebarService
  ) { }

  public authUsingPhoneAndPassword(phone_number: string, password: string): Observable<APIResponse> {
    return this.api.post('admin/login', { phone_number, password })
      .map(response => response.json() as APIResponse)
      .do(response => this.processShop(response.data));
  }

  private processShop(response) {
    if (typeof response.user !== null && typeof response.user.shops !== "undefined") {
      this.sidebarService.recreateShopsSubject();
      localStorage.removeItem('shops');

      response.user.shops.forEach(shop => {
        this.sidebarService.addShop(shop.label, shop.id);
      });
    }
  }

}

import { Http, Request, RequestOptions, RequestOptionsArgs, Response, XHRBackend } from "@angular/http"
import { Injectable } from "@angular/core"
import { Subject } from "rxjs/Subject";
import { Observable } from "rxjs/Observable";

import "rxjs/add/operator/catch"
import "rxjs/add/observable/throw"
import "rxjs/add/operator/map"


@Injectable()
export class HttpInterceptor extends Http {

  public loadingState: Subject<boolean> = null;

  constructor(
    backend: XHRBackend,
    options: RequestOptions,
    public http: Http
  ) {
    super(backend, options);
    this.loadingState = new Subject();
  }

  // public request(url: string|Request, options?: RequestOptionsArgs): Observable<Response> {
  //   return super.request(url, options)
  //     .do(data => this.loadingState.next(true))
  //     .catch(this.handleError)
  // }

  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    this.loadingState.next(true);

    return super.request(url, options)
      .do(data => this.loadingState.next(false))
      .catch(this.handleError);
  }

  public handleError = (error: Response) => {
    this.loadingState.next(false);
    return Observable.throw(error)
  }


}

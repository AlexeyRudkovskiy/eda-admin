import { Pipe, PipeTransform } from '@angular/core';
import {Option} from "../models/option";

@Pipe({
  name: 'options'
})
export class OptionsPipe implements PipeTransform {

  transform(value: Option[], filter: string): any {
    if (!value || !filter) {
      return value;
    }

    filter = filter.toLowerCase();

    return value
      .filter(option => {
        return (
          option.name.toLowerCase().indexOf(filter) > -1 ||
          (
            option.description !== null ? option.description.toLowerCase().indexOf(filter) > -1 : false
          )
        )
      });
  }

}
